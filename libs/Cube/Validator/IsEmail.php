<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */
require_once 'Cube/Validator/Interface.php';

class Cube_Validator_IsEmail implements Cube_Validator_Interface
{

    public function __construct($params)
    {
    }

    public function validate($value)
    {
		if (!preg_match('/^[^@ ]+@[^@ ]+\.[^@ \.]+$/',$value)) return false;
		return true;
    }
}
