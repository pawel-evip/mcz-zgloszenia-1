<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
function br2nl($text)
{
    $text = str_replace("<br />","",$text);
    $text = str_replace("<br>","",$text);
    return $text;
}

function runDefined($const)
{
	$const = strtoupper($const);
	$const = str_replace(' ', '_', $const);
   	$code = "return $const;"; 
   	$function = create_function('', $code); 
   	$result = $function();
   	if ($result === $const) return false;
   	return $result;
}

function profileName($value)
{
	return strtr($value, array('&oacute;' => 'o',
								  '&quot;' => '',
								  '&circ;' => 'z',
								  ' ' => '_',
								  '"' => '',
								  'ą' => 'a',
								  'ś' => 's',
								  'ć' => 'c',
								  'ę' => 'e',
								  'ó' => 'o',
								  'ń' => 'n',
								  'ł' => 'l', 
								  'ź' => 'z',
								  'ż' => 'z',
								  'A' => 'a',
								  'Ś' => 's',
								  'Ć' => 'c',
								  'Ę' => 'e',
								  'Ó' => 'o',
								  'Ń' => 'n',
								  'Ł' => 'l', 
								  'Ź' => 'z',
								  'Ż' => 'z',
								  '!' => '1',
								  '@' => '2',
								  '#' => '3',
								  '$' => '4',
								  '%' => '5',
								  '^' => '6',
								  '&' => '7',
								  '*' => '8',
								  '(' => '9',
								  ')' => '10'
								  ));
}

function dbPrice($price)
{
	$price = trim(str_replace(',', '.', $price));
	return number_format($price, 2, '.', '');
}	

function price($price)
{
	$price = trim(str_replace(',', '.', $price));
	return number_format($price, 2, ',', ' ');
}

function mysql_fetch_all($r)
{
	
	$result = array();
	while ($row = mysql_fetch_assoc($r))
		$result[] = $row;
	//echo mysql_errno() . ": " . mysql_error(). "\n"; 	
	//echo 'R='.$r;
	return $result;	
}

function getTableClass()
{
	static $a = 0;
	if (!$a) {
		$a++;
		return '';
	}
	$a = 0;
	return ' class="alt"';
}

function now($dayOnly = false)
{
    return date($dayOnly ? 'Y-m-d' : 'Y-m-d H:i:s');
}

function stripMagicQuotes($arr)
{
    foreach ($arr as $k => $v) {
        $arr[$k] = is_array($v) ? stripMagicQuotes($v) : stripslashes($v);
    }
    return $arr;
}

function checkMagicQuotes()
{
    if (get_magic_quotes_gpc()) {
        if (!empty($_GET))     $_GET = StripMagicQuotes($_GET);
        if (!empty($_POST))    $_POST = StripMagicQuotes($_POST);
        if (!empty($_REQUEST)) $_REQUEST = StripMagicQuotes($_REQUEST);
        if (!empty($_COOKIE))  $_COOKIE = StripMagicQuotes($_COOKIE);
    }    
}

function tags($var)
{
	return strip_tags($var);
}

function entities($var)
{
	return htmlentities($var, ENT_QUOTES, 'UTF-8');
}

function clear($var)
{
	return entities(tags($var));
}

function getParam($var, $return = null, $clear = true) 
{
	if (isset($var)) {
		if ($clear) $var = clear($var);
		return $var;
	} else {
		return $return;
	}
}

function allowHTML($var, $allowed = null)
{
	$var = nl2br($var);
	if (is_null($allowed)) $allowed = '<p><span><pre><b><i><u><br><div><table><tr><td><tbody><tfoot><thead><img><h1><h2><h3><h4><h5><h6><a>';	
	$var = strip_tags($var, $allowed);
	$var = addslashes($var);
	return $var;
}

function getSelectLanguages($lang = null, $all = true)
{
	$languages = Cube_Registry::get('systemLanguages');
	$result = null;
	
	if ($all) $result .= '<option value="all">wszystkie</option>';
	
	foreach ($languages as $k => $v)
	{
		if ($k == $lang) $result .= '<option value="'.$k.'" selected="selected">'.$v.'</option>';
		else $result .= '<option value="'.$k.'">'.$v.'</option>';
	}
	return $result;
}

function show_tree($rows,$root=false)
{
	//funkcja tworzy drzewo do wyswietlania (zmiana kolejnosci wierszy).
	//$rows	- tablica zawierajaca wiersze drzewa posortowane w kolejnosci poziom,(id lub name lub pos).
	//$root - true - wyswietla rowniez korzeń. false  - opuszcza karzen, zaczyna od dzieci korzenia.
	
	$parent_id=array();
		
	for ($i=0;$i < count($rows);$i++)
		for ($j=$i+1;$j<count($rows);$j++)
			if ($rows[$i]['id']==$rows[$j]['parent_id'])
				$parent_id[$rows[$i]['id']][]=$rows[$j]['id'];
			

	$tree=array();
	for ($i=0;$i < count($rows);$i++)
	{
		//jezeli wiecej niz jeden korzen (parent_id=1)
		if($root)
            $col='id';
        else
            $col='parent_id';
        if ($rows[$i][$col]==1) 
		{
			$id=$rows[$i]['id'];
			$depth=0;
			get_children($id,$parent_id,$rows,$tree,$depth);
			$tree+=$tree;
		}
	}
		
	return $tree;
}