<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
class Cube_Registry extends ArrayObject
{
	private static $_registry = null;
	
	public function __construct() {}
	public function __clone() {}
	
	public static function getInstance()
	{
        if (self::$_registry === null) {
			self::$_registry = new self();
        }

      	return self::$_registry;	
	}
	
    public static function get($index)
    {
        $instance = self::getInstance();

        if (!$instance->offsetExists($index)) {
            require_once 'Cube/Exception.php';
            throw new Cube_Exception("Brak zarejestrowanego wpisu: '$index'");
        }

        return $instance->offsetGet($index);
    }

    public static function set($index, $value)
    {
        $instance = self::getInstance();
        $instance->offsetSet($index, $value);
    }

    public static function isRegistered($index)
    {
        if (self::$_registry === null) {
            return false;
        }
        return self::$_registry->offsetExists($index);
    }

    public function offsetExists($index)
    {
        return array_key_exists($index, $this);
    }

}
