<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
require_once 'Cube/Filter/Interface.php';

class Cube_Filter_AddSlashes implements Cube_Filter_Interface
{

    public function __construct($params)
    {
    }

    public function filter($value)
    {
        return addslashes($value);
    }
}
