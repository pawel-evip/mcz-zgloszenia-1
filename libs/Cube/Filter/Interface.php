<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */ 
interface Cube_Filter_Interface
{
	public function __construct($params);
	public function filter($value);
}

?>
