<?php 



	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		if (isSet($this->doc_name))
		{
			//echo 'NAME='.$this->doc_name;
			//jezeli dodajemy plik z serwera ftp (opcja nowe pliki).
			$doc_html='<div><label for="movie"><strong>Dokument:</strong></label> <input type="text" name="movie" id="movie" value="'.$this->doc_name.'" /></div>';
		}	
		else
			$doc_html='<div><label for="photo"><strong>Dokument:</strong></label> <input type="file" name="photo" id="photo" /></div>';
			
echo'<h1>Dodaj / Zarządzaj danymi</h1>';
				
echo '

<h2>Dodaj dane</h2>
<form id="add_photo" action="admin,media,adddoc.html" method="post" enctype="multipart/form-data">
<fieldset>
<div><label for="cid"><strong>Kategoria:</strong></label> <select name="cid"><option value="0">- wybierz kategorię -</option>'.$this->categoriesList.'</select></div>
<div><label for="title"><strong>Tytuł:</strong></label> <input type="text" name="title"id="title" value="'.$this->title.'" /></div>'
.$doc_html.
'<div><label for="description">Krótki opis:</label> <input type="text" name="description" id="description" value="'.$this->description.'" /></div>
<div>* Pola <strong>pogrubione</strong> są wymagane.</div>
<div><input type="submit" name="submit" id="submit" class="submit" value="DODAJ" /><input type="reset" name="reset" id="reset" class="submit" value="WYCZYŚĆ" /></div>
</fieldset>
</form>';
		

echo '<br /><br /><br />
<h2>Zarządzaj danymi</h2>

		<form action="admin,media,search,cid_'.$this->cid.'.html" method="post" style="width:auto; margin:0 0 10px 0; float:left; ;">Wyszukaj zdjęcie (id , tytuł, nazwa pliku, rozmiar):<br /><input type="text00" name="search" value="'.$this->search->getSearchString().'" style="width:150px; margin:5px 0 0 0;" /><input type="submit" name="submit" id="submit" style="margin:0;" class="submit" value="SZUKAJ" />
		</form>

	<form id="show" action="admin,media,files.html" method="post" style="width:auto; margin:0 0 10px 0; float:right; ">Wyświetl z kategorii:<br /><select name="cid" style="margin:5px 0 0 0;"><option value="0">- WSZYSTKIE KATEGORIE -</option>'.$this->categoriesList.'</select><input type="submit" name="submit" class="submit" id="submit" style="margin:0;" value="POKAŻ" />
	</form>
';
				
		echo '
		<table cellspacing="0">
					<thead>
						<tr>
							<td class="tocenter"><a href="'.$this->order->getlink('id').',cid_'.$this->cid.'.html" >ID'.$this->order->getImageStatus('id').'</a></td>
							<td><a href="'.$this->order->getlink('title').',cid_'.$this->cid.'.html" >Tytuł'.$this->order->getImageStatus('title').'</a></td>
							<td class="tocenter"><a href="'.$this->order->getlink('date').',cid_'.$this->cid.'.html" >Data dodania'.$this->order->getImageStatus('date').'</a></td>
							<td class="tocenter">Kategoria</td>							
							<td class="tocenter"><a href="'.$this->order->getlink('filename').',cid_'.$this->cid.'.html" >Nazwa pliku'.$this->order->getImageStatus('filename').'</a></td>
							<td class="tocenter"><a href="'.$this->order->getlink('size').',cid_'.$this->cid.'.html" >Rozmiar'.$this->order->getImageStatus('size').'</a></td>			
							<td class="toright">Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->docs;
			if (count($rows) < 1) {
				echo '<tr><td colspan="6">Nie odnaleziono żadnych dokumentów w wybranej kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}	
					//$cat;
					//foreach($this->categories as $c)
					//	if($c['id'] == $r['cid'])
					//		$cat = $c['name'];
							
					$size=round($r['size']/1024);
					//$size=round($r['size']);
					$size=number_format($size, 0, ',', ' ');
					$size.=' kB'; 
					echo '<tr'.$class.'>
							<td class="tocenter">'.$r['id'].'<br /><a href="admin,media,deletedoc,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
							<td><div style="width:200px; overflow:auto;">'.$r['title'].'</div></td>
							<td class="tocenter">'.date('Y-d-m',$r['add_date']).'</td>
							<td class="tocenter">'.$r['cat'].'</td>							
							<td class="tocenter"><a href="public/media/docs/'.$r['filename'].'" target="_blank">Podgląd</a></td>
							<td class="tocenter">'.$size.'</td>
							<td class="toright"><a href="admin,media,editdoc,id_'.$r['id'].'.html">Edytuj</a></td>
						</tr>';
				}		
			}
		
			echo '</tbody></table>
			
		';
		
	}
?>
