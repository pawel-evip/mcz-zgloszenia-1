<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			echo '<form action="admin,media,insert.html" method="post" enctype="multipart/form-data">
				<fieldset>
					<legend>Dodaj</legend>
					<div><label for="title"><span class="b">Tytuł:</span></label><input type="text" name="title" value="'.$this->row['title'].'" /></div>
					<div><label for="file"><span>Plik:</span></label><input type="file" name="file" id="file" class="input" /></div>
					<div><p>* Pola <strong>pogrubione</strong> są wymagane.</p></div>
					<div>
						 <input type="submit" name="submit" id="submit" value="dodaj" class="submit-first" />			
						 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
					</div>
				</fieldset>
			</form>';
	}
?>
