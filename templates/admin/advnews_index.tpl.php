<?php	
		
if (isset($this->errors))
{
	echo '<div class="errorBox"><ul>';
	foreach ($this->errors as $error)
	{
		echo "<li>$error</li>";
	}
	echo '</ul></div>';
}

if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';
else
{ 			
//wyszukiwanie
	//$search=null;
	$value_form='';	//w tej zmiennej jest zachowany tekst ktory byl wyszukiwany

//sortowanie
	$botton_state= array('idstate' => 1, 'titlestate' =>1, 'adddatestate' => 1,'authorstate' => 1,'categorystate' => 1);
	$botton_sort= array('idstate' => null, 'titlestate' =>null, 'adddatestate' => null,'authorstate' => null,'categorystate' => null);
	

	
	$action=$this->_request->getAction();
	//echo '$action='.$action;
	//echo _request->getParam()
	if(strstr($action,'sort'))
	{
		//echo "Akcja OK\n";
		switch ($this->_request->getAction())
		{
			case 'sortID' : $key='idstate'; break;
			case 'sortTitle' : $key='titlestate'; break;
			case 'sortAddDate' : $key='adddatestate'; break;
			case 'sortAuthor' : $key='authorstate'; break;
			case 'sortCategory' : $key='categorystate'; break;
			
		}
		
		$botton_state[$key]=$this->_request->getParam($key);
		if($botton_state[$key]==1)
		{
			$botton_sort[$key]=' <img src="templates/admin/images/up.png" alt="" />';
			$botton_state[$key]=2;
		}	
		elseif($botton_state[$key]==2)
		{
			$botton_sort[$key]=' <img src="templates/admin/images/down.png" alt="" />';
			$botton_state[$key]=1;
		}	
	}
	
	//w sortowaniu uwzględnij tekst z pola search.
	if ($this->_request->isPost())	
	{
		//echo 'Paramert='.$this->_request->getPost('search');
		$search=$this->_request->getPost('search');
		$value_form=$search;		
	}
	else
		$search=$this->search;	
	
	if(IsSet($this->search))
		$value_form=$search;
	

echo '<h1>Aktualności</h1>

	  <form action="admin,advnews,search.html" method="post" class="searchinside">
	  Wyszukaj aktualność (id lub tytuł)<br /><br />
				<input type="text00" name="search" value="" class="wpisz2" /><input type="submit" name="submit" id="submit" value="Szukaj" style="position:relative; top:-15px;" class="submit" />
		 </form><br /><br /><br />
	     ';

echo '
<table id="tab-zlecenie">
			<thead>
				<tr>
					<td><a href="admin,advnews,sortID,idstate_'.$botton_state['idstate'].',search_'.$search.'.html">ID'.$botton_sort['idstate'].'</a></td>
					<td><Jezyk</td>
					<td><a href="admin,advnews,sortAddDate,adddatestate_'.$botton_state['adddatestate'].',search_'.$search.'.html">Data Dodania'.$botton_sort['adddatestate'].'</a></td>
					<td><a href="admin,advnews,sortAuthor,authorstate_'.$botton_state['authorstate'].',search_'.$search.'.html">Autor'.$botton_sort['authorstate'].'</a></td>
					<td><a href="admin,advnews,sortTitle,titlestate_'.$botton_state['titlestate'].',search_'.$search.'.html">Tytuł'.$botton_sort['titlestate'].'</a></td>
					<td><a href="admin,advnews,sortCategory,categorystate_'.$botton_state['categorystate'].',search_'.$search.'.html">Kategoria'.$botton_sort['categorystate'].'</a></td>		
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';
	
	$rows = (array)$this->rows;
	
	if (count($rows) < 1) echo '<tr><td colspan="7">Nieodnaleziono żadnych aktualności w bazie.</td></tr>';
	else {	
		$ids = array();
		foreach($rows as $k => $r) 
		{
			if (isset($ids[$r['id']]))
				$rows[$ids[$r['id']]]['name'][] = $r['name'];
			else {
				if (!is_array($r['name'])) {
					$rows[$k]['name'] = array($r['name']);
				}	
				$ids[$r['id']] = $k;
			}	
		}
	
		$ids = array();
		foreach($rows as $r) 
		{
			if (isset($ids[$r['id']])) continue;
		
			$ids[$r['id']] = 1;
			$class = getTableClass();
			
			if ($r['language'] == 'all') $r['language_img'] = 'wszystkie';
			else $r['language_img'] = '<img src="languages/flags/'.$r['language'].'.gif">';
			
			$cats = '';
			foreach ($r['name'] as $v) 
				$cats .= $v.'<br />';	
				
			echo '	<tr'.$class.'>
					<td>'.$r['id'].'<br /><a href="admin,advnews,delete,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
					<td>'.$r['language_img'].'</td>
					<td>'.date('d.m.Y H:i', $r['add_date']).'</td>
					<td>'.$r['author'].'</td>
					<td>'.$r['title'].'</td>
					<td>'.$cats.'</td>	
					<td><a href="admin,advnews,edit,id_'.$r['id'].'.html">Edytuj</a></td>
				</tr>';
		}		
	}

	echo '</tbody><tfoot><tr><td colspan="7"></td></tr></tfoot></table>';
}	
?>
