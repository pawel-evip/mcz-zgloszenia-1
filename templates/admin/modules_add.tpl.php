<?php

$modules = (array)$this->modules;
echo '<h1>Lista modułów możliwych do zainstalowania:</h1><table id="administrate" class="clear">
			<thead>
				<tr>
					<td>Nazwa modułu</td>
					<td>Opis</td>
					<td>Informacje dodatkowe</td>
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';
	
	if (sizeof($modules['valid']) < 1) 
		echo '<tr><td colspan="5">Nie odnaleziono żadnych poprawnych modułów przygotowanych do instalacji.</td></tr>';
	else 
	{
		foreach($modules['valid'] as $row) 
		{
			$class = getTableClass();
			echo '<tr'.$class.'>
					<td>'.$row['name'].'</td>
					<td>'.$row['description'].'</td>
					<td>'.$row['info'].'</td>
					<td><a href="admin,modules,install,name_'.$row['name'].'.html">instaluj</a></td>
				</tr>';
		}		
	}

	echo '</tbody></table>';

echo '<h2>Lista nieprawidłowych modułów:</h2><table id="administrate" class="clear">
			<thead>
				<tr>
					<td>Nazwa błędnego pliku</td>
				</tr>
			</thead>
			<tbody>';
	
	if (sizeof($modules['invalid']) < 1) echo '<tr><td colspan="1">Nie odnaleziono żadnych niepoprawnych modułów.</td></tr>';
	else {
		foreach($modules['invalid'] as $row) 
		{
			$class = getTableClass();
			echo '<tr'.$class.'>
					<td>'.$row.'</td>
				</tr>';
		}		
	}

	echo '</tbody></table>';
?>
