<?php 
if($this->csv==1)
{
	//pobierz plik na dysk lokalny
	header('Content-disposition: attachment; filename=Lista_zgloszen.csv');
    header('Content-type: text/tab-separated-values');
    readfile('public/csv/Lista_zgloszen.csv');
    exit;    //tu mus byc exit,a nie return
}


define('STRING_SEPARATOR', '^'); //separator uzyty do stringu search

$pages = $this->pages;			// obiekt operujący na stronnicowaniu wyników, opis poniżej z przykladem
$rows=$this->rows_pages;

//wyszukiwanie
$value_search='';	//w tej zmiennej jest zachowany tekst ktory byl wyszukiwany
$value_start_date='';	//w tej zmiennej jest zachowany tekst ktory byl wyszukiwany
$value_end_date='';	//w tej zmiennej jest zachowany tekst ktory byl wyszukiwany
		
$search=$this->search;
$start_date=$this->start_date;
$end_date=$this->end_date;

if(IsSet($search))
	$value_search=$search;
		
if(IsSet($start_date))
	$value_start_date=$start_date;
			
if(IsSet($end_date))
	$value_end_date=$end_date;
		
		

if (isset($this->errors)) {
	echo '<div class="errorBox"><ul>';
	foreach ($this->errors as $error)
	{
		echo "<li>$error</li>";
	}
	echo '</ul></div>';
		
	echo return_echo_search($value_search,$value_start_date,$value_end_date);
	return;
}

if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';	
else
{
		//sortowanie
		$column=$this->column;	//sortowana kolumna UWAGA pierwsa kolumna ma nr 0
		$status=$this->state;  //status sortowanej kolumny 1-UP, 2-DOWN
		
		//sortowanie
		$column=$this->column;	//sortowana kolumna UWAGA pierwsa kolumna ma nr 0
		$status=$this->state;  //status sortowanej kolumny 1-UP, 2-DOWN
				
		//echo 'STATUS='.$status;
		//echo 'COLUMN='.$column;
		//echo 'SEARCH='.$search;
		
		//0 - kolumna nie jest sortowana, 1 - sortowanie DOWN, 2- sortowanie UP
		$botton_state=array(1,1,0,1,1,1,1,1,1,0);	//UWAGA pierwsa kolumna ma nr 0. UWAGA bierzemy pod uwage wszystkie kolumny z tabeli nawet te nie sortowane (1 - sortowana, 0, nie sortowana).
		$botton_sort= array(null,null,null,null,null,null,null,null,null,null);

		if( $status == 1 )
		{
			$botton_sort[$column]=' <img src="templates/admin/images/up.png" alt="" />';
			$botton_state[$column]=2;
		}
		elseif($status == 2)
		{
			$botton_sort[$column]=' <img src="templates/admin/images/down.png" alt="" />';
			$botton_state[$column]=1;
		}	
		
		$search_string=$search.STRING_SEPARATOR.$start_date.STRING_SEPARATOR.$end_date;
		
		if(is_null($this->print))
		{
					//echo '<a style="cursor:pointer;" onclick="javascript:window.open(\'/zgloszenia-print.html\',\'_o_\',\'width=500, height=430, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no\')">Podgląd wydruku</a>';
			echo '<a class="print" onclick="javascript:window.open(\'/admin,notifications,printindex,column_'.$column.',state_'.$botton_state[$column].',search_'.$search_string.'.html\',\'_o_\',\'width=800, height=500, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no\')">Podgląd wydruku</a>';
		}
		else
		{
			//dla podgladu nie ma stronicowania.
			$rows = (array)$this->rows;
		}
		if(is_null($this->print))
		{		
			echo'<a href="admin,notifications,pdfindex,column_'.$column.',state_'.$botton_state[$column].',search_'.$search_string.'.html" class="pdf">PDF</a>';
			echo'<a href="admin,notifications,csvindex,column_'.$column.',state_'.$botton_state[$column].',search_'.$search_string.'.html" class="pdf">CSV</a>';
			echo return_echo_search($value_search,$value_start_date,$value_end_date);
			
			 echo'<br clear="all"/><br /><br />
		<table id="tab-notification" cellspacing="1" style="font-size:10px; width:800px;">
			<thead>
				<tr>
					<td><a href="admin,notifications,sort,column_0,state_'.$botton_state[0].',search_'.$search_string.'.html">ID'.$botton_sort[0].'</a></td>
					<td><a href="admin,notifications,sort,column_1,state_'.$botton_state[1].',search_'.$search_string.',start_0.html">Login'.$botton_sort[1].'</a></td>
					<td>Imię Nazwisko</td>
					<td><a href="admin,notifications,sort,column_3,state_'.$botton_state[3].',search_'.$search_string.',start_0.html">Tytuł'.$botton_sort[3].'</a></td>
					<td><a href="admin,notifications,sort,column_4,state_'.$botton_state[4].',search_'.$search_string.',start_0.html">Status'.$botton_sort[4].'</td>
					<td><a href="admin,notifications,sort,column_5,state_'.$botton_state[5].',search_'.$search_string.',start_0.html">Typ'.$botton_sort[5].'</td>
					<td><a href="admin,notifications,sort,column_6,state_'.$botton_state[6].',search_'.$search_string.',start_0.html">Priorytet'.$botton_sort[6].'</td>
					<td><a href="admin,notifications,sort,column_7,state_'.$botton_state[7].',search_'.$search_string.',start_0.html">Jednostka'.$botton_sort[7].'</td>
					<td><a href="admin,notifications,sort,column_8,state_'.$botton_state[8].',search_'.$search_string.',start_0.html">Data'.$botton_sort[8].'</a></td>
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';
		}
		else
		{
			echo '<table id="tab-notification" cellspacing="1" style="font-size:10px; width:800px;">
			<thead>
				<tr>
					<td class="tocenter" >ID</td>
					<td> Login</td>
					<td>Imię Nazwisko</td>
					<td> Tytuł</td>
					<td> Status</td>
					<td> Typ</td>
					<td> Prioryter</td>
					<td> Jednostka</td>
					<td class="tocenter">Data</td>
				</tr>
			</thead>
			<tbody>';
		}	
		
			if (count($rows) < 1 ) {
				echo '<tr><td colspan="4">Nie odnaleziono żadnych wpisów.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					$class = getTableClass();		
					echo '<tr'.$class.'>
						<td>'.$r['id'].'</td>
						<td>'.$r['login'].'</td>
						<td>'.$r['name'].'	'.$r['surname'].'</td>
						<td>'.$r['subject'].'</td>
						<td>'.$r['status'].'</td>
						<td>'.$r['type'].'</td>
						<td>'.$r['priority'].'</td>
						<td>'.$r['unit'].'</td>
						<td>'.date('Y-m-d',$r['add_date']).'</td>';
						if(is_null($this->print))
							echo '<td><a href="admin,notifications,edit,id_'.$r['id'].'.html">Edytuj</a><br />
								 <a href="admin,notifications,delete,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąc wybrany element?\')" >usuń</a></td>';
					echo '</tr>';
					
					
				}		
			}
		
			echo '</tbody></table>';
		
	if (!(isset($this->errors)))
	{	
		//Nie pagowania jezeli jest blad
		if(!is_null($this->print))
		{
			//Na podgladzie jest przycisk drukuj
			echo '<link rel="stylesheet" type="text/css" href="templates/admin/styles/print.css" />';
			echo '<a href="javascript:window.print()" class="print">drukuj</a>';
			
				
		}	
		else 
		{
			//dla podgladu wydruku nie ma stronicowania.
			if ($pages->hasPrev() || $pages->hasNext()) 
			{ 
				echo '<div style="display:block; text-align:center; overflow:hidden; padding:10px; border-top:1px solid #36c;">';
			
				if ($pages->hasPrev()) {
					echo $pages->getPrevLink('<a href="'.$this->template.'">Poprzednia</a>');
				} else {
				echo '<a href="#">Poprzednia</a>';
				}
			
				echo ''.$pages->getPages() .'';
			
				if ($pages->hasNext()) {
					echo $pages->getNextLink('<a href="'.$this->template.'">Następna</a>');
				} else {
					echo '<a href="#">Następna</a>';
				}
				echo '</div><br />';
			}	
		}
	}		
	}


function return_echo_search($value_search,$value_start_date,$value_end_date)
{
	$return_echo=null;
	$return_echo.=
			'<form action="admin,notifications,search.html" method="post" style="float:left; width:420px;">
<table cellspacing="0" style="float:left; text-align:left; border:1px solid #ccc;width:420px;">
	<tr>
	<td style="background:#fff; padding:2px 0 2px 3px;"><label>Wpisz frazę</label><br /><input type="text" name="search" value="'.$value_search.'" style="width:100px;" /></td>
	<td style="background:#fff; padding:2px 0 2px 3px;"><label for="start_date">Data (początek):</label><br /><input type="text" name="start_date" id="start_date" value="'.$value_start_date.'" style="width:100px;" /></td>
	<td style="background:#fff; padding:2px 0 2px 3px;"><label for="end_date">Data (koniec):</label><br /><input type="text" name="end_date" id="end_date" value="'.$value_end_date.'" style="width:100px;" /></td>
	<td style="background:#fff; padding:2px 5px 2px 3px;"><input type="submit" name="submit" id="submit" value="Szukaj" class="submit" style="margin:13px 0 0 0;" />
	</tr>
	<tr><td colspan="4" style="text-align:left; background:#fff; padding:2px 0 2px 3px;"><font color="#ff0000">Format dla pól z datą:DD-MM-RRRR </font></td></tr>
</table>	
		     </form>';

	return $return_echo;

}
?>