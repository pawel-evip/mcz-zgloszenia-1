<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		
		
		echo '<h1 class="center"><a href="admin,entry,categories.html">Przejdź do kategorii Korzeń.</a></h1>
		<h1 class="center"><a href="javascript:history.back()">Powrót (przejdź wstecz).</a></h1>
		<form id="show" action="admin,entry,categories.html" method="post">
			<fieldset>
		<div><label for="cid"><span class="b">Skocz do kategorii:</span></label>
			<select name="pid">
			<option value="0"> wybierz kategorię </option>
						'.$this->categoriesList.'
			</select>
			<input type="submit" name="submit" id="submit" value="pokaż" class="submit-show" />		
		</div>
		</fieldset>
		</form>
	
	<br /> <br />
		
		<form id="add_cat" action="admin,entry,addcat.html" method="post">
			<fieldset>
				<legend>Dodaj kategorię do kategorii '.$this->currentPath.'</legend>';
		
				
		echo '<div><label for="name"><span class="b">Nazwa:</span></label><input type="text" name="name" id="name" value="" /></div>
		<div><label for="description"><span >Opis:</span></label><textarea rows="3" name="description" id="description"></textarea></div>
		<div><p>Pola pogrubione są wymagane.</p></div>
				<div>
					<input type="hidden" class="hidden" name="pid" value="'.$this->pid.'">
					 <input type="submit" name="submit" id="submit" value="dodaj" class="submit-first input" />			
					 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit input" />
				</div></fieldset>
		</form><br /><br />';

			echo '<h1 class="center">Zarządzaj podkategoriami kategorii '.$this->currentPath.':</h1>
			<table id="administrate" class="clear">
					<thead>
						<tr>
							<td>ID</td>
							<td>Nazwa</td>
							<td>Ilość klipów</td>
							<td>Status</td>	
							<td>Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			
			if (count($rows) < 1) {
				echo '<tr><td colspan="6">Nie odnaleziono żadnych kategorii.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					if ($a == 1) {
						$class = ' class="alt"';
						$a = 0;
					} else {
						$class = null;
						$a++;
					}	
					
					if ($r['active']) {
						$status = '<span class="green">aktywna</span>';
						$status_action = ' | <a href="admin,entry,deactive,id_'.$r['id'].',pid_'.$this->pid.'.html">Deaktywuj</a>';
					} else {
						$status = '<span class="red">nieaktywna</span>';
						$status_action = ' | <a href="admin,entry,active,id_'.$r['id'].',pid_'.$this->pid.'.html">Aktywuj</a>';
					} 				
					 				
					echo '			<tr'.$class.'>
							<td>'.$r['id'].'<br /><a href="admin,entry,deletecat,id_'.$r['id'].',pid_'.$this->pid.'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąć wybrany element?\')">Usuń</a></td>
							<td>'.$r['name'].'</td>
							<td>'.$r['p_amount'].'</td>
							<td>'.$status.'</td>
							<td><a href="admin,entry,categories,pid_'.$r['id'].'.html">Pokaż podkategorie</a>
							<br />
							<a href="admin,entry,index,cid_'.$r['id'].'.html">Pokaż klipy</a>
							<br />
							<br /><a href="admin,entry,editcat,id_'.$r['id'].',pid_'.$this->pid.'.html">Edytuj</a> '.$status_action.'</td>
						</tr>';
				}		
			}
		
			echo '</tbody></table>';
	}

?>
