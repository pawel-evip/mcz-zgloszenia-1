<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {	
		echo '
		<form id="add-admin" action="admin,users,edit,id_'.$this->id.'.html" method="post">
			<fieldset>
				<legend>Edycja danych "'.$this->login.'"</legend>
				<div><label for="name"> <span><strong>Imię:</strong></span></label>  <input class="short" name="name" id="name" type="text" value="'.$this->name.'" /></div>
				<div><label for="surname"> <span><strong>Nazwisko:</strong></span></label>  <input class="short" name="surname" id="surname" type="text" value="'.$this->surname.'" /></div>
				<div><label for="email"> <span><strong>Email:</strong></span> </label>  <input class="short" name="email" id="email" type="text" value="'.$this->email.'" /></div>	
				<div><label for="phone"> <span>Telefon:</span> </label>  <input class="short" name="phone" id="phone" type="text" value="'.$this->phone.'" /></div>	
				<div><p>* Pola <strong>pogrubione</strong> są wymagane.</p></div>
				<div>
					<input type="submit" class="submit-first" value="dodaj" />
					<input type="reset" class="submit" value="anuluj" />
				</div>
				</fieldset>									
		</form>';
	}

?>
