<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			$wysiwyg = null;
			if ($this->row[$this->wys] == '1') $wysiwyg = ' checked="checked"';
			
			if ($this->_session->getRole() == 'administrator') {
				echo '<form action="admin,settings.html" method="post">
					<fieldset>
						<legend>Konfiguracja</legend>
						<div><label for="wysiwyg" class="long"><span>Używaj edytora WYSIWYG:</span></label><input type="checkbox" class="check" name="wysiwyg" value="1"'.$wysiwyg.' /></div>
						<div>	 	
							 <input type="hidden" class="hidden" name="act" value="1" />
							 <input type="submit" name="submit" id="submit" value="zapisz" class="submit-first-long" />			
							 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
						</div>
					</fieldset>
				</form>';
			} else {
				$languages = null;
				foreach ($this->systemLanguages as $k => $v) 
				{
					if ($this->row['default_language'] == $k) $languages .= '<option value="'.$k.'" selected="selected">'.$v.'</option>';	
					else $languages .= '<option value="'.$k.'">'.$v.'</option>';
				}
				
				$modules = null;
				foreach ($this->allowed_modules as $v) 
				{
					if ($this->row['default_module'] == $v['name']) $modules .= '<option value="'.$v['name'].'" selected="selected">'.$v['name'].'</option>';	
					else $modules .= '<option value="'.$v['name'].'">'.$v['name'].'</option>';
				}			
				
				echo '<form action="admin,settings.html" method="post">
					<fieldset>
						<legend>Konfiguracja</legend>
						<div><label for="default_module" class="long"><span>Startowy moduł:</span></label><select class="short" name="default_module">'.$modules.'</select></div>
						<div><label for="default_language" class="long"><span>Domyślny język:</span></label><select class="short" name="default_language">'.$languages.'</select></div>
						<div><label for="wysiwyg" class="long"><span>Używaj edytora WYSIWYG:</span></label><input type="checkbox" class="check" name="wysiwyg" value="1"'.$wysiwyg.' /></div>
						<div>	 	
							 <input type="hidden" class="hidden" name="act" value="1" />
							 <input type="submit" name="submit" id="submit" value="zapisz" class="submit-first-long" />			
							 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
						</div>
					</fieldset>
				</form>';
				
				/*foreach ($this->systemLanguages as $k => $v) 
				{
					echo '<form action="admin,settings.html" method="post">
						<fieldset>
							<legend>'.$v.' - konfiguracja zależna od języka.</legend>
							<div><label for="keywords_'.$k.'" class="long"><span>Meta keywords:</span></label><textarea class="short" name="keywords_'.$k.'" rows="4">'.$this->row['keywords_'.$k].'</textarea></div>
							<div><label for="description_'.$k.'" class="long"><span>Meta description:</span></label><textarea class="short" name="description_'.$k.'" rows="4">'.$this->row['description_'.$k].'</textarea></div>
							<div><label for="title_'.$k.'" class="long"><span>Meta title:</span></label><textarea class="short" name="title_'.$k.'" rows="4">'.$this->row['title_'.$k].'</textarea></div>
							<div><label for="footer_'.$k.'" class="long"><span>Stopka:</span></label><textarea class="short" name="footer_'.$k.'" rows="4">'.$this->row['footer_'.$k].'</textarea></div>
							<div>	 
								<input type="hidden" class="hidden" name="lang" value="'.$k.'" />
								 <input type="submit" name="submit" id="submit" value="zapisz" class="submit-first-long" />			
								 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
							</div>
						</fieldset>
					</form>';
				}*/
			}
	}
?>
