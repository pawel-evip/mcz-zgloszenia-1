<?php 

if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';	
	else
	{
		echo '<h1>Zawartość zgłoszenia</h1>';
		
		if(!is_null($this->print)) echo '<strong>Zgłoszenie Miedziowego Centrum Zdrowia</strong><br /><br />';
		
		if(is_null($this->print))
			echo '<a class="print" onclick="javascript:window.open(\'/admin,notifications,printedit,id_'.$this->id.'.html\',\'_o_\',\'width=800, height=500, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no\')">Podgląd wydruku</a>';
		
	
	
	//ODPOWIEDZ NA ZGŁOSZENIE
	if(is_null($this->print)) echo '<a href="admin,notifications,pdfedit,id_'.$this->id.'.html" class="pdf">PDF</a>';
	
	$n = (array)$this->notifications;

	echo '<br clear="all"/><br /><strong>Zawartość zgłoszenia</strong><br />';
	echo '<table cellspacing="1" id="nowa">';
	echo '<tr><th width="260">Użytkownik zgłaszający login:</th><td>'.$n['login'].'</td></tr>';
	echo '<tr><th>Użytkownik zgłaszający imię:</th><td>'.$n['name'].'</td></tr>';
	echo '<tr><th>Użytkownik zgłaszający nazwisko:</th><td>'.$n['surname'].'</td></tr>';
	echo '<tr><th>Jednostka organizacyjna:</th><td>'.$n['unit'].'</td></tr>';
	echo '<tr><th>Temat:</th><td>'.$n['subject'].'</td></tr>';
	echo '<tr><th>Przyczyna powstania:</th><td>'.$n['reason'].'</td></tr>';
	echo '<tr><th>Priorytet:</th><td>'.$n['priority'].'</td></tr>';
	echo '<tr><th>Status:</th><td>'.$n['status'].'</td></tr>';
	echo '<tr><th>Typ:</th><td>'.$n['type'].'</td></tr>';
	echo '<tr><th>Data dodania:</th><td>'.date('Y-m-d H:i:s',$n['add_date']).'</td></tr>';
	echo '<tr><th>Opis:</th><td>'.$n['description'].'</td></tr>';
	echo '</table>';
	
	
	echo '<br clear="all"/><br /><strong>Historia zgłoszenia</strong><br />';
	$rows = (array)$this->messages;
	foreach($rows as $r)
	{
		
		echo '<table cellspacing="1" id="nowa">';
		echo '<tr><th width="50%">'.$r['login'].' ('.date('Y-m-d H:i:s',$r['add_date']).')</th><th style="text-align:right;" width="50%"><span>Status:</span> '.$r['status'].' | <span>Priorytet:</span> '.$r['priority'].'</th></tr>';
		echo '<tr><td colspan="2">'.$r['description'].'</td></tr></table>';
		
	
	
	}
	
	if(is_null($this->print))
	{
	$rows = (array)$this->status;
	foreach($rows as $r)
	{ 
		//zbuduj selecta dla statusów
		if ($r['id']==1 && ($n['id_status']==1 || $n['id_status']==2 ) )
		{
			//Nie wyswietlaj statusu przyjety jezeli zgloszenie ma status przyjety lub otwarty
			;	
		}
		else if( $r['id']==2 && $n['id_status']==1 )
		{
			//jezeli zgloszenie ma status przyjety to domyslnym zgloszeniem bedzie zgloszenie otwarte
			$select_status.='<option value='.$r['id'].' selected >'.$r['title'].'</option>';	
		}	
		else if ($n['id_status']==$r['id'])
			$select_status.='<option value='.$r['id'].' selected >'.$r['title'].'</option>';
		else
			$select_status.='<option value='.$r['id'].' >'.$r['title'].'</option>';
	}
	
	$rows = (array)$this->prioritys;
	foreach($rows as $r)
	{ 
		//zbuduj selecta dla priorytetów
		if ($n['id_priority']==$r['id'])
			$select_priority.='<option value='.$r['id'].' selected >'.$r['title'].'</option>';
		else
			$select_priority.='<option value='.$r['id'].' >'.$r['title'].'</option>';
	}
	
	 echo '<form action="admin,notifications,edit,id_'.$this->id.'.html" id="add_message" method="post">
		<fieldset>
			<legend>Odpowiedz na zgłoszenie </legend>
			<div><label for="description"><span>Opis:</span></label></div>
			<div><textarea class="tiny" id="description" type="text" class="long" name="description" value=""></textarea></div>
			<div>
			<div><label for="id_status"><span>Status:</span></label><select id="id_status" name="id_status">'. 
						$select_status
						.'</select></div>
			<div>			
			<div><label for="id_priority"><span>Priorytet:</span></label><select id="id_priority" name="id_priority">'. 
						$select_priority
						.'</select></div>
				<input type="submit" name="submit" id="submit" value="dodaj" class="submit-first" />			
				<input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
			</div>
		</fieldset>
	 </form>';

	} 
	
     if(!is_null($this->print))
	{
			echo '<link rel="stylesheet" type="text/css" href="templates/admin/styles/print.css" />';
			echo '<a href="javascript:window.print()" class="print">drukuj</a>';
			
				
	}	
}
?>
