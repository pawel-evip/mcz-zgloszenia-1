<?php 


	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {		
		echo '
		<form id="add_photo" action="admin,advgallery,deletecat,id_'.$this->id.'.html" method="post">
			<fieldset>
				<legend>Usuń kategorię</legend>';
				
		echo '	<div><p><b>UWAGA !</b> Po usunięciu kategorii zostaną automatycznie usunięte wszystkie jej podkategorie i zdjęcia w tych podkategoriach !</p></div>
				<div><p>Zanim usuniesz kategorię musisz zadecydować co zrobić ze zdjęciami do niej należacymi:</p></div>
				<div><label for="act"><span>Usuń zdjęcia:</span></label><input type="radio" class="check" name="act" id="act" value="delete" checked="checked" /></div>
				<div><label for="act"><span>Przenieś:</span></label><input type="radio" class="check" name="act" id="act" value="przenies" /></div>
		<div><label for="cat"><span class="b">Przenieś do:</span></label>
			<select name="cat">
			<option value="0"> -- WYBIERZ KATEGORIĘ -- </option>
						'.$this->categoriesList.'
			</select>
		</div>
				<div>
					 <input type="submit" name="submit" id="submit" value="usuń" class="submit-first input" />			
					 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit input" />
				</div></fieldset>
		</form>';
	}
 
?>
