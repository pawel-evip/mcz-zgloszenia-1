<?php 

if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';	
	else
	{

		// DODAJ NOWY TYP ZDARZENIA
		 echo '<form action="admin,notifications,priority.html" method="post">
				<fieldset>
					<legend>Dodaj nowy priorytet: </legend>
					<div><label for="addtype"><span>Priorytet:</span></label><input type="text" class="short" name="title" value="" /></div>
					<div>
					 <input type="submit" name="submit" id="submit" value="dodaj" class="submit-first" />			
					 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
					</div>
				</fieldset>
		  </form>';
		  
		 // WYSWIETL TABELE Z typami zdarzeń
		  $rows = (array)$this->rows;
		  
		echo '<h1>Zarządzaj aktualnymi priorytetami:</h1>
		<form action="admin,notifications,pospriority.html" class="in-table" method="post">		
		<table id="administrate" class="clear">
					<thead>
						<tr>
						<td>ID</td>
							<td>Tytuł</td>
							<td>Pozycja</td>	
							<td>Akcja</td>
						</tr>
					</thead>
					<tbody>';
		
			$rows = (array)$this->rows;
			if (count($rows) < 1 ) {
				echo '<tr><td colspan="4">Nie odnaleziono żadnych wpisów.</td></tr>';
			} else {	
				foreach($rows as $r) 
				{
					$class = getTableClass();				 	
					echo '<tr'.$class.'>
					<td>'.$r['id'].'</td>
						<td>'.$r['title'].'</td>
						<td><input type="text" class="in-table-short" style="width: 40px;text-align: center;" name="pos_'.$r['id'].'" value="'.$r['pos'].'" /></td>
						<td><a href="admin,notifications,deletepriority,id_'.$r['id'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz usunąc wybrany element?\')" >usuń</a>
						<a href="admin,notifications,editpriority,id_'.$r['id'].'.html">edytuj</a></td>
						
					</tr>';
					
				}		
			}
		
			echo '</tbody></table>
			<input type="submit" value="aktualizuj" class="submit-first" /></form><br /><br /><br /><br />';	
	}
?>
