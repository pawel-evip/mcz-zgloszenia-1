<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';			
	else {
			$technologie = array('grafika', 'html', 'css', 'php', 'mysql', 'flash', 'hosting', 'email');
			echo '<form action="admin,realizacje,edit,id_'.$this->row['id'].'.html" method="post">
				<fieldset>
					<legend>Edytuj realizację</legend>
					<div><label for="name"><span class="b">Nazwa:</span></label><input type="text" name="name" value="'.$this->row['name'].'" /></div>			
					<div><label for="href"><span class="b">Adres http://:</span></label><input type="text" name="href" value="'.$this->row['href'].'" /></div>
					<div><label for="description"><span>Opis:</span></label></div>
					<div><textarea class="tiny" name="description" rows="8">'.$this->row['description'].'</textarea></div>
					';

						foreach ($technologie as $k => $v)
						{
							if (array_search($v, $this->row['params']) !== false) echo '<div><input type="checkbox" class="check" name="technologie[]" value="'.$v.'" checked="checked" /> '.$v.'</div>';
							else echo '<div><input type="checkbox" class="check"  name="technologie[]" value="'.$v.'" /> '.$v.'</div>';	
						}
						
					echo '
					<div><p>* Pola <strong>pogrubione</strong> są wymagane.</p></div>
					<div>
						 <input type="hidden" class="hidden" name="id" value="'.$this->row['id'].'" />
						 <input type="submit" name="submit" id="submit" value="aktualizuj" class="submit-first" />			
						 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
					</div>
				</fieldset>
			</form>';
	}
?>
