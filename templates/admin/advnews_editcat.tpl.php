<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {

		echo '
		<form id="add_cat" action="admin,advnews,editcat,id_'.$this->id.'.html" method="post">
			<fieldset>
				<legend>Edytuj kategorię</legend>';
		
		
		if ($this->addable) $addable = ' checked="checked"';
		else $addable = '';
				
		echo '
		<div><label for="name"><span class="b">Nazwa:</span></label><input type="text" name="name" id="name" value="'.$this->name.'" /></div>
		<div><label for="max_chars_main"><span class="">Max. znaków - strona główna:</span></label><input type="text" name="max_chars_main" id="max_chars_main" value="'.$this->max_chars_main.'" /></div>
		<div><label for="max_chars_advnews"><span class="">Max. znaków - aktualności:</span></label><input type="text" name="max_chars_advnews" id="max_chars_advnews" value="'.$this->max_chars_advnews.'" /></div>	
		<div><p>Pola pogrubione są wymagane.</p></div>
				<div>
					 <input type="submit" name="submit" id="submit" value="aktualizuj" class="submit-first input" />			
					 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit input" />
				</div></fieldset>
		</form>';
	}

?>
