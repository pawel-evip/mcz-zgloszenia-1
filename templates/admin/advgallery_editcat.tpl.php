<?php 

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {

		echo '
		<form id="add_cat" action="admin,advgallery,editcat,id_'.$this->id.',pid_'.$this->pid.'.html" method="post">
			<fieldset>
				<legend>Edytuj kategorię</legend>';
		
		
		if ($this->addable) $addable = ' checked="checked"';
		else $addable = '';
				
		echo '<div><label for="name"><span class="b">Nazwa:</span></label><input type="text" name="name" id="name" value="'.$this->name.'" /></div>
		<div><p>Pola pogrubione są wymagane.</p></div>
				<div>
					 <input type="submit" name="submit" id="submit" value="aktualizuj" class="submit-first input" />			
					 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit input" />
				</div></fieldset>
		</form>';
	}

?>
