<?php
	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		$logged = null;
		if ($this->row['logged'] == '1') $logged = ' checked="checked"';
		
		echo '<form action="admin,articles,edit,id_'.$this->row['id'].'.html" method="post">
		<fieldset>
			<legend>Aktualizuj podstronę/artykuł</legend>
			<div><label for="author"><span class="b">Autor:</span></label><input type="text" name="author" id="author" class="short" value="'.$this->row['author'].'" /></div>			
			<div><label for="title"><span class="b">Tytuł:</span></label><input type="text" name="title" id="title" value="'.$this->row['title'].'" /></div>
			<div><label for="meta_title"><span>Meta title:</span></label><input type="text" name="meta_title" value="'.$this->row['meta_title'].'" /></div>		
			<div><label for="logged"><span>Tylko zalogowani:</span></label><input type="checkbox" class="check" name="logged" value="1"'.$logged.' /></div>	
			<div><label for="contents"><span class="b">Treść:</span></label></div>
			<div> <textarea name="contents" id="contents" class="tiny" rows="36">'.$this->row['contents'].'</textarea></div>
			<div><p>Pola pogrubione są wymagane.</p></div>
			<div>
				<input type="hidden" class="hidden" name="id" value="'.$this->row['id'].'" />
				<input type="submit" name="submit" id="submit" value="aktualizuj" class="submit-first" />			
				<input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
			</div>	
		</fieldset>
		</form>';
	}
?>
