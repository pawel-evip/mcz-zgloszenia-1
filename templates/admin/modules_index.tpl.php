<?php

echo '<h1>Zarządzaj modułami</h1>
<table id="administrate" class="clear">
			<thead>
				<tr>
					<td>#</td>
					<td>Nazwa modułu</td>
					<td>Opis</td>
					<td>Informacje dodatkowe</td>
					<td>Status</td>
					<td>Akcja</td>
				</tr>
			</thead>
			<tbody>';
	$rows = (array)$this->rows;
	
	if (sizeof($rows) < 1) echo '<tr><td colspan="7">Nie odnaleziono żadnych modułów w bazie.</td></tr>';
	else {
		foreach($rows as $row) 
		{
			$class = getTableClass();
			
			if ($row['active']) 
				$status = '<span class="green">aktywny</span><br /><a href="admin,modules,deactive,id_'.$row['id'].'.html">deaktywuj</a>';
			else 
				$status = '<span class="red">nieaktywny</span><br /><a href="admin,modules,active,id_'.$row['id'].'.html">aktywuj</a>';
			
			/*$access = '<form class="access" action="admin,modules,access,id_'.$row['id'].'.html" method="post">
				<fieldset>
				<select name="access">';
			switch ($row['admin_access'])
			{
				case 'root': $access .= '<option value="root" selected="selected">root</option>
										<option value="administrator">administrator</option>
										<option value="none">none</option>
										'; 
							 break;
				case 'administrator': $access .= '<option value="root">root</option>
										<option value="administrator" selected="selected">administrator</option>
										<option value="none">none</option>
										'; 
									 break;
				default: $access .= '<option value="root">root</option>
										<option value="administrator">administrator</option>
										<option value="none" selected="selected">none</option>
										'; 
			}
			$access .= '</select><br /> <input type="submit" class="sub" value="aktualizuj" /></fieldset></form>';
			*/
			if ($row['name'] != 'modules' && $row['name'] != 'admins' && $row['name'] != 'settings')
				$unaction = '<a href="admin,modules,uninstall,name_'.$row['name'].'.html" onclick="return confirm(\'Czy jesteś pewien, że chcesz odinstalować wybrany element?\')">odinstaluj</a>';
			else {
				$status = '<span class="green">aktywny</span>';
				$unaction = 'brak';
			}		
			echo '<tr'.$class.'>
					<td>'.$row['id'].'</td>
					<td>'.$row['name'].'</td>
					<td>'.$row['description'].'</td>
					<td>'.$row['info'].'</td>
					<td>'.$status.'</td>
					<td>'.$unaction.'</td>
				</tr>';
		}		
	}

	echo '</tbody></table>';

?>
