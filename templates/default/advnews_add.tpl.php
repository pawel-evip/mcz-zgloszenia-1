<?php

	/* NIE RUSZAC */
	$errors = $this->errors;			// tablica errorów (pole wymagane nie wypelnione itd..) lub 0 jesli wszystko ok
	if ($errors == '') $errors = 0;
														
	$message = $this->message;		// komunikat o pomyslnym dodaniu komentarza
	$row = $this->row;	// dane podane do formularza (pamięta jeśli nie podano jakiegoś wymaganego pola.)
	$max = $this->max;  // $max = maksymalna ilośc znaków w tresci komenatrza	
	$id = $this->id; // nr ID newsa do ktorego dodajemy komentarz 											
	/* END NIE RUSZAC */



	/* sprawdza czy istnieja errory, jesli tak wyswietla je w postaci:
		<div class="errorBox">
			<ul>
				<li>wiadomosc 1</li>
				<li>wiadomosc 2</li>
				<li>wiadomosc n</li>
			</ul>
		</div>
		
		oczywiscie to sobie modyfikujesz jak chesz.		
	
	*/	
	if ($errors != 0) {
		echo '<div class="errorBox"><ul>';
		foreach ($errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	/*
		Sprawdza czy operacja dodania sie powiodla, jesli tak wysiwtla komunikat w postaci:
			<div class="message"><p>wiadomosc</p></div>
			
		jesli nie to wyswietla formularz	
	*/
	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {
		echo '<form action="aktualnosci,dodaj_komentarz,'.$id.'.html" method="post">
		<fieldset>
			<legend>Dodaj komentarz</legend>
			<div>
				<label for="author"><span class="b">Autor:</span></label>
				<input type="text" name="author" value="'.$row['author'].'" />
			</div>			
			<div>
				<label for="contents"><span class="b">Treść:</span></label>
				<textarea name="contents" rows="8">'.$row['contents'].'</textarea></div>
			<div>
				<p>
					* Pola <strong>pogrubione</strong> są wymagane.<br />
					* Maksymalna ilość znaków treści to '.$max.'<br /> 
					* Obsługa BBCode = [b]pogrubione[/b] [u]podkreślone[/u] [i]pochylone[/i] <br /> 					
				</p>
			</div>
			<div>
				 <input type="hidden" class="hidden" name="id" value="'.$id.'" />
				 <input type="submit" name="submit" id="submit" value="dodaj" class="submit-first" />			
				 <input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
			</div>	
		</fieldset>
		</form>';
	}
	
	/*
		FORMULARZ DODAWANIA KOMENTARZA
		- może wyglądać jak chcesz, ale:
		
			1. musi być przesłany metodą post (method="post")
			2. musi wykonywać akcje dodaj_komentarz.html (action="dodaj_komentarz.html")
			3. nazwa pola z autorem musi nazywać się 'author'
			4. nazwa pola z treścia musi nazywać się 'contents'
	
	*/

?>
