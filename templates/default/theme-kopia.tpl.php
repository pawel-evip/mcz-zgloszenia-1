<?php 
	/*
		THEME.TPL.PHP to główny plik szablonu, tzn. w tym pliku definiuje sie strukturę strony.
		W tym pliku (i wszystkich plikach kontrolerów, czyli np: news_index.tpl.php) są dostępne następujące zmienne:
		
			1. $controller = zmiena zwracająca całą zawartość akcji, czyli zawartośc wygenerowana 
							 np przez news_index.tpl.php, czyli jakby przez "podstronę", moduł.
							 THEME.TPL.PHP musi być tak napisany, aby w połączeniu z wygenerowaną zawartością podstron
							 (czyli pozostalych plikow z katalogu teplates/default/) tworzyły kompletną strone.
							 Mam nadzieje ze mnie rozumiesz, chodzi o to że plik każda akcja (np: wyswietlenie newsa, wszystkich newsow, podstrony)
							 jest generowana przez inny plik, np: news_index.tpl.php, news_view.tpl.php i wygenerowana 
							 zawartosc jest wrzycana w miejsce $controller t theme.tpl.php
							 UWAGA:  $controller uzywaj tylko w THEME.TPL.PHP
			
			2. $meta = wyświetla znaczniki meta keywords, meta description i title wygenerowane na podst. danych 
					   ustawionych w panelu i zalezne od języka.
					   UWAGA:  $meta uzywaj tylko w THEME.TPL.PHP	
			
			3. $footer = wyświetla zawartość stopki ustaloną w panelu adm.
			
			4. $moduleName = nazwa obecnie uruchomionego modułu
			5. $language = obecny język w postaci krótkiej, np: 'pl', 'en'
			6. $styles = jeśli w katalogu 'templates/default/styles/' znajduje się plik style_JEZYK.css  
						 np style_pl.css i style_en.css to zmienna $styles zawiera znacznik <link rel...> 
						 tego własnie stylu. Używane przy korzystaniu z wielu języków, np w style.css są domyślne 
						 obrazki tła, a w style_pl.css i style_en.css są nadpisane style, zawierające odpowieniki 
						 obrazków dla danego języka, wtedy własnie ten styl zostanie załadowany.
						 
			7. $languagesDir = zwraca 'templates/default/languages/OBECNY_JEZYK/'
			8. $stylesDir = zwraca 'templates/default/styles/'
			9. $imagesDir = zwraca 'templates/default/images/'
			10.	$currentURL = zawiera kawałek ścieżki odpowiadający za obecne żadanie. 
							  za pomocą $currentURL mozemy sprawdzić jaka akcja wykonywana jest obecnie,
							  wykorzystuje się przy generowaniu menu.
							  
			11. Zmienne typu $menu_NRMENU gdzie NRMENU to ID odpowiadający menu z panelu administracyjnego, 
				przykład wykorzystania poniżej.	
				Wszystkie zmienne $menu_X są dwuwymiarowymi tablicami, które po potraktowaniu foreach'em posiadaja:
				$menu_X['label'] = wyświetlany tytuł odnośnika
				$menu_X['href'] = adres odnośnika			  			   				 
				 
	*/
	
	//$latestNewsBlock = $this->getBlock('latestNews');	
	//$latestNews = (array)$latestNewsBlock->getLatest();	// $latestNews = dwuwymiarowa tablica zawierająca ostatnie X newsów (X do ustawienia w panelu)
													// jak zwykle - trzeba potraktować ją foreach'em
	//$jsMenu = $this->getBlock('JsMenu');	
														
?>

<?php 
/*
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php echo str_replace('&oacute;', 'ó', $meta); ?>
<script type="text/javascript" src="templates/default/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="templates/default/js/jquery.lightbox.min.js"></script>
</head>
<body>
<h3>eVip CMS v1.2.1 (25.10.2008)</h3>
<p>Brak szablonu. Musisz wgrać szablon!<br /></p>
<p><a href="admin.html">Panel administracyjny</a></p>
</body>
</html>*/
?>

<?php

if (!($this->_session->isLoggedIn()))
{

echo '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Nazwa aplikacji - System Obsługi Zgłoszeń: system autoryzacji</title>
	<link rel="stylesheet" type="text/css" href="templates/admin/styles/auth_style.css" />
</head>
<body>

	<div id="login">
	
	<form action="auth,login.html" method="post">
	<table cellpadding="0" cellspacing="0">
	<tr><td>login</td><td><input type="text" name="username" value="" /></td></tr>
	<tr><td>hasło</td><td><input type="password" name="password" value="" /></td></tr>
	<tr><td></td><td><input type="submit" value="Wyślij" class="submit" /><input type="reset" value="Wyczyść" class="submit" /></td></tr>
	</table>
	</form>
	
	</div>

</body>
</html>';

}

else
{



echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="templates/default/styles/main.css" rel="stylesheet" type="text/css" />
<script src="templates/default/js/fla.js" type="text/javascript"></script>';
echo str_replace('&oacute;', 'ó', $meta); 
echo '<script type="text/javascript" src="templates/default/scripts/jquery.min.js"></script>
<script type="text/javascript" src="templates/default/scripts/all_scripts.js"></script>
<script type="text/javascript" src="templates/default/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="templates/default/js/jquery.lightbox.min.js"></script>
<script type="text/javascript" src="templates/default/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="public/css/jquery.Jcrop.css" type="text/css" />
<script src="templates/default/js/flowplayer-3.2.4.min.js"></script>
</head>

<body>';

	echo 'zalogowany jako:'.$this->_session->getUsername().' 
	  <a href="auth,logout.html">wyloguj</a>';

	$rozwijane = $this->getBlock('RozwijaneMenu');
	$rozwijane = $rozwijane->get();	
	$menu2 = $rozwijane[0]['links'];
	
	//print_r($rozwijane);
	//exit;
			
	echo '<!--menu begin-->
		<div id="menu">
			<div class="in">
			';

	function checkActivity($currentURL, $href, $r, $row)
	{
		/*if ((strpos($currentURL, 'aktualnosci') !== false && strpos($href, 'aktualnosci') !== false) ||
				 	((strpos($currentURL, 'wydarzenia.html') !== false || strpos($currentURL, 'wydarzenia,') !== false) && strpos($href, 'wydarzenia.html') !== false)  ||
				 	(strpos($currentURL, 'wydarzenia_ostatnio') !== false && strpos($href, 'wydarzenia_ostatnio') !== false) ||
				 	(strpos($currentURL, 'sklepy') !== false && strpos($href, 'sklepy') !== false) ||
				 	(strpos($currentURL, 'promocje,') !== false && strpos($href, 'promocje.') !== false) ||
				 	(strpos($currentURL, 'dodaj_promocje') !== false && strpos($href, 'dodaj_promocje') !== false) ||
				 	(strpos($currentURL, 'galeria') !== false && strpos($href, 'galeria') !== false) ||
				 	(strpos($currentURL, 'media_51') !== false && strpos($href, 'media_51') !== false) ||
				 	(strpos($currentURL, 'media.') !== false && strpos($href, 'media.') !== false))
			*/
					 return true;
		
		// przypadek dla eventów
		//if ($r->getController() == 'events' && $r->getAction() == 'view') {
		//	if ($row['event']['past'] == 2 && $row['event']['active'] == 1) { // ostatnio
		//		if (strpos($href, 'wydarzenia_ostatnio') !== false) 
		//			return true;
		//	} else {
		//		if (strpos($href, 'wydarzenia.') !== false) 
		//			return true;
		//	}
		//}
					 
		//return false;			 	
	}
			
	$main = null;
	foreach ($rozwijane as $menu) 
	{
			$show = null;
			if ($currentURL == $menu['href'] || checkActivity($currentURL, $menu['href'], $this->_request, $row))
			{
				$menu2 = $menu['links'];
				$show = '<a href="'.$menu['href'].'" title="'.$menu['extra'].'" class="mark">'.$menu['name'].'</a>';
			} else {	
				$show = '<a href="'.$menu['href'].'" title="'.$menu['extra'].'">'.$menu['name'].'</a>';	
				foreach($menu['links'] as $l) // sprawdz, moze jednak jakis z podlinkow inncyh niz wyzej
				{
					if ($currentURL == $l['href'] || checkActivity($currentURL, $l['href'], $this->_request, $row))
					{
						$menu2 = $menu['links'];
						$show = '<a href="'.$menu['href'].'" title="'.$menu['extra'].'" class="mark">'.$menu['name'].'</a>';
					}
				}			
			}
			echo $show;	
	}
	
	echo '</div>
	</div>
	<!--menu end-->
	';
	
	// echo $controller;
	
	 echo '
	  <!--content2 begin-->
		<div class="content">
		'.$controller.'
		</div>
		<!--content2 end-->';
	
	

$toc = $this->getBlock('GetToc');
echo $toc->get(1);
			
echo '
</body>
</html>';

}
?>
