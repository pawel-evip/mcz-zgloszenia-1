<?php

	if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message"><p>'.$this->message.'</p></div>';		
	else {	
	
	echo '
		<form id="change-password" action="zmien_haslo.html" method="post">
		
			<table class="tabela" cellspacing="1">
				<thead>
					<tr><td colspan="2">Zmień hasło</td></tr>
				</thead>
				
				<tbody>
					<tr><td width="150"><label for="pass">Hasło:</label></td><td><input class="short" name="pass" type="password" value="'.$this->row['pass'].'" /></td></tr>
					<tr><td><label for="repass">Powtórz hasło:</label></td><td><input class="short" name="repass" type="password" value="'.$this->row['repass'].'" /></td></tr>
					<tr><td></td><td><input type="submit" class="submit" value="zmień" /><input type="reset" class="submit" value="anuluj" /></td></tr>
				</tbody>
			</table>
								
		</form>';
		
	echo '
		<form id="change-profile" action="zmien_profil.html" method="post">
		
			<table class="tabela" cellspacing="1">
				<thead>
					<tr><td colspan="2">Edycja danych</td></tr>
				</thead>
				
				<tbody>
					<tr><td width="150"><label for="name"><strong>Imię:</strong></label></td><td><input class="short" name="name" id="name" type="text" value="'.$this->name.'" /></td></tr>
					<tr><td><label for="surname"><strong>Nazwisko:</strong></label></td><td><input class="short" name="surname" id="surname" type="text" value="'.$this->surname.'" /></td></tr>
					<tr><td><label for="email"><strong>Email:</strong></label></td><td><input class="short" name="email" id="email" type="text" value="'.$this->email.'" /></td></tr>
					<tr><td><label for="phone">Telefon:</label></td><td><input class="short" name="phone" id="phone" type="text" value="'.$this->phone.'" /></td></tr>
					<tr><td colspan="2">* Pola <strong>pogrubione</strong> są wymagane.</td></tr>
					<tr><td></td><td><input type="submit" class="submit" value="zmień" /><input type="reset" class="submit" value="anuluj" /></td></tr>
				</tbody>
			</table>
			
		</form>';	
	}

?>
