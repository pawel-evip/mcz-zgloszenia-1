function byID(id)
{
	var obj;
	if (document.getElementById) obj = document.getElementById(id);
	else if (document.all) obj = document.all[id];
	else if (document.layers) {
		obj 	  = document.layers[id];
		obj.style = document.layers[id];	
	}
	return obj;	
}


function isValid()
{
	//alert('Pole "Temat" jest wymagane');
	if (byID('subject').value == '') { alert('Pole "Temat" jest wymagane'); return false; }
	//if (byID('description').value == '') { alert('Pole "Opis" jest wymagane'); return false; }
	//if (byID('reason').value == '') { alert('Pole "Przyczyna powstania" jest wymagane'); return false; }
	
	
	
	//sprawdzenie selecta
	if (byID('type').options[byID('type').selectedIndex].value == '') {
		alert('Pole "Typ" jest wymagane'); return false;
	}	

	return true;
}

function isEmail(email)
{
	 if (email.indexOf(' ')==-1 
	      && 0<email.indexOf('@')
	      && email.indexOf('@')+1 < email.length
	 ) return true;
	 return false;
}
