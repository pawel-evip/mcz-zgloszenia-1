<?php 

if (isset($this->errors)) {
		echo '<div class="errorBox"><ul>';
		foreach ($this->errors as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}

	if (!is_null($this->message)) echo '<div class="message">'.$this->message.'</div>';	
	else 
	{
	
		echo '<h1>Zawartość zgłoszenia</h1>';
		
		if(!is_null($this->print)) echo '<strong>Zgłoszenie Miedziowego Centrum Zdrowia</strong><br /><br />';
		
		if(is_null($this->print))
			echo '<a class="print" onclick="javascript:window.open(\'/zgloszenia-print-edit_'.$this->id.'.html\',\'_o_\',\'width=800, height=500, toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no\')">Podgląd wydruku</a>';
		
		
	// ODPOWIEDZ NA ZGŁOSZENIE
	$n = (array)$this->notifications;
	
	if (strcmp($n['login'],$this->_session->getUsername()))
	{
		//wyjdz jezeli inny user niz na zgloszeniu
		return;
	}	
		
	echo '<a href="zgloszenia-pdf-edit_'.$this->id.'.html" class="pdf">PDF</a>';
	echo '<table cellspacing="1" class="dane2">
		  <thead>
			<tr><td colspan="2">Dane o zgłoszeniu</td></tr>
		  </thead>
		  <tbody>
			<tr><td width="180">Jednostka organizacyjna:</td><td>'.$n['unit'].'</td></tr>
			<tr><td>Temat:</td><td>'.$n['subject'].'</td></tr>
			<tr><td>Przyczyna powstania:</td><td><div style="overflow:auto; height:100px;">'.$n['reason'].'</div></td></tr>
			<tr><td>Priorytet:</td><td>'.$n['priority'].'</td></tr>
			<tr><td>Status:</td><td>'.$n['status'].'</td></tr>
			<tr><td>Typ:</td><td>'.$n['type'].'</td></tr>
			<tr><td>Data dodania:</td><td>'.date('Y-m-d H:i:s',$n['add_date']).'</td></tr>
			<tr><td>Opis:</td><td><div style="overflow:auto; height:100px;">'.$n['description'].'</div></td></tr>
		  </tbody>
		  </table>
		  <table cellspacing="1" class="dane">
		  <thead>
			<tr><td colspan="2">Użytkownik zgłaszający</td></tr>
		  </thead>
		  <tbody>
			<tr><td width="80">Login:</td><td>'.$n['login'].'</td></tr>
			<tr><td>Imię:</td><td>'.$n['name'].'</td></tr>
			<tr><td>Nazwisko:</td><td>'.$n['surname'].'</td></tr>
		  </tbody>
		  </table>
		  <br clear="all" />';
	
	
	echo '<h3>Historia zgłoszenia</h3>';
	$rows = (array)$this->messages;
	foreach($rows as $r)
	{
		echo '<div class="post">
			  <span class="user">'.$r['login'].' - <span>'.date('Y-m-d H:i:s',$r['add_date']).'</span></span><br />
			  '.$r['description'].'
			  </div>';
		
	
	
	}
	if(is_null($this->print))
	{
		if ($n['id_status']==1 || $n['id_status']==2)
		{
		//Message mozliwy tylko dla statusu przyjęty lub otwarty
			echo '
			<h3>Odpowiedz na zgłoszenie</h2>
			<form action="zgloszenia_'.$this->id.'.html" id="add_message" method="post">
			<table cellspacing="0" class="zgloszenie">
			<tr>
				<td><textarea class="tiny" id="description" type="text" name="description" value=""></textarea></td>
			</tr>
			<tr>
				<td><input type="submit" name="submit" id="submit" value="dodaj" class="submit" /><input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
			</tr>
			</table>
			</form>';
		}
	 }
	 
	 if(!is_null($this->print))
	{
			echo '<link rel="stylesheet" type="text/css" href="templates/default/styles/print.css" />';
			echo '<a href="javascript:window.print()" class="print">drukuj</a>';
			
				
	}	
	 
	 
	/*$rows = (array)$this->status;
	foreach($rows as $r)
	{ 
		//zbuduj selecta dla statusów
		if ($n['id_status']==$r['id'])
			$select_status.='<option value='.$r['id'].' selected >'.$r['title'].'</option>';
		else
			$select_status.='<option value='.$r['id'].' >'.$r['title'].'</option>';
	}
	
	$rows = (array)$this->prioritys;
	foreach($rows as $r)
	{ 
		//zbuduj selecta dla priorytetów
		if ($n['id_priority']==$r['id'])
			$select_priority.='<option value='.$r['id'].' selected >'.$r['title'].'</option>';
		else
			$select_priority.='<option value='.$r['id'].' >'.$r['title'].'</option>';
	}
	
	 echo '<form action="zgloszenia_'.$this->id.'.html" id="add_message" method="post">
		<fieldset>
			<legend>Odpowiedz na zgłoszenie </legend>
			<div><label for="description"><span>Opis:</span></label><textarea class="long" id="description" type="text" class="long" name="description" value="" </textarea></div>
			<div>
			<div><label for="id_status"><span>Status:</span></label><select id="id_status" name="id_status"'. 
						$select_status
						.'</select></div>
			<div>			
			<div><label for="id_priority"><span>Priorytet:</span></label><select id="id_priority" name="id_priority"'. 
						$select_priority
						.'</select></div>
				<input type="submit" name="submit" id="submit" value="dodaj" class="submit-first" />			
				<input type="reset" name="reset" id="reset" value="wyczyść" class="submit" />
			</div>
		</fieldset>
	 </form>'; */

  
}
?>
