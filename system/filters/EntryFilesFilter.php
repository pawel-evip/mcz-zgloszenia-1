<?php

require_once 'Cube/Filter/Abstract.php';

class EntryFilesFilter extends Cube_Filter_Abstract
{
	public function filter()
	{	
		$filters    = array('filename' => 'Clear','title' => 'Clear');
		$validators = array();
		$messages   = array(); 	
		$this->_process($filters, $validators, $messages);	
	}
}
					 
?>
