<?php

require_once 'Cube/Filter/Abstract.php';

class MediaEditCategoryFilter extends Cube_Filter_Abstract
{
	public function filter()
	{	
		$filters    = array('name' => 'Clear', 'description' => 'Clear', 'id'=>'Clear');
		$validators = array('name' => 'Required');
		$messages   = array('name' => 'Pole "Tytuł" jest wymagane'); 	
		$this->_process($filters, $validators, $messages);	
	}
}
					 
?>
