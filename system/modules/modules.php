<?php

$moduleInfo = array(
			'name' 			=> 'modules',
			'admin_access' 	=> 'root',
			'access' 		=> 'none',
			'description' 	=> 'Instalator modułów',
			'info' 			=> 'v1.0, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'add';

$moduleInstall = array();
$moduleUninstall = array();

?>
