<?php

$moduleInfo = array(
			'name' 			=> 'advgallery',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Moduł zaawansowanej galerii',
			'info' 			=> 'v1.0, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'categories';
$moduleActions[] = 'settings';


$moduleInstall = array();
$moduleInstall[] = 'INSERT INTO config VALUES (null, "per_page", "16", "advgallery")';
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advgallery_categories` (
  `id` int(11) NOT NULL auto_increment,
  `parent_id` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `p_amount` int(11) NOT NULL default '0',
  `s_amount` int(11) NOT NULL default '0',  
  `path` text NOT NULL default '',  
  `active` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM;";
					
$moduleInstall[] = "CREATE TABLE IF NOT EXISTS `advgallery_photos` (
  `id` int(11) NOT NULL auto_increment,
  `cid` int(11) NOT NULL default '0',
  `add_date` int(16) NOT NULL default '0',
  `filename` varchar(255) NOT NULL default '',
  `title` varchar(255) NOT NULL default '',
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM;";
					
/* CONFIG */					
$moduleInstall[] = 'INSERT INTO advgallery_categories VALUES (null, 0, "Korzeń", 0, 0, "", 1)';
					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `advgallery_categories`";
$moduleUninstall[] = "DROP TABLE `advgallery_photos`";
$moduleUninstall[] = 'DELETE FROM config WHERE segment = "advgallery"';

?>
