<?php

$moduleInfo = array(
			'name' 			=> 'articles',
			'admin_access' 	=> 'administrator',
			'access' 		=> 'guest',
			'description' 	=> 'Podstrony/artykuły',
			'info' 			=> 'v1.1, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'insert';
//$moduleActions[] = 'toc';
//$moduleActions[] = 'addtoc';

$moduleInstall = array();
$moduleInstall[] = "CREATE TABLE `articles` (
					  `id` int(11) NOT NULL auto_increment,
					  `add_date` int(16) NOT NULL default '0',
					  `views` int(11) NOT NULL default '0',
					  `meta_title` varchar(255) NOT NULL default '',
					  `author` varchar(255) NOT NULL default '',
					  `title` varchar(255) NOT NULL default '',
					  `contents` text NOT NULL, 
					  `logged` tinyint(1) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
/*$moduleInstall[] = "CREATE TABLE `articles-toc` (
					  `id` int(11) NOT NULL auto_increment,
					  `name` varchar(255) NOT NULL default '',
					  `contents` text NOT NULL, 
					  `language` varchar(3) NOT NULL default '',
					  `logged` tinyint(1) NOT NULL default '0',
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM;";
*/					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `articles`";
$moduleUninstall[] = "DROP TABLE `articles-toc`";

?>
