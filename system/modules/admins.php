<?php

$moduleInfo = array(
			'name' 			=> 'admins',
			'admin_access' 	=> 'root',
			'access' 		=> 'none',
			'description' 	=> 'Administratorzy serwisu',
			'info' 			=> 'v1.0, Michal Daniel, www.icube.pl'
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'add';
$moduleActions[] = 'passwd';

$moduleInstall = array();
$moduleUninstall = array();

?>
