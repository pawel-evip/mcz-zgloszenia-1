<?php

$moduleInfo = array(
			'name' 			=> 'notifications',
			'admin_access' 	=> 'root',
			'access' 		=> 'none',
			'description' 	=> 'Zgłoszenia',
			'info' 			=> 'v1.0,Switez, www.evipstudio.pl'			
			);
			
$moduleActions = array();
$moduleActions[] = 'index';
$moduleActions[] = 'status';
$moduleActions[] = 'type';
$moduleActions[] = 'priority';
$moduleActions[] = 'unitcity';
$moduleActions[] = 'settings';
//$moduleActions[] = 'passwd';

$moduleInstall[] = "CREATE TABLE `notification` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_user` int(11) unsigned NOT NULL,
  `id_status` int(11) unsigned NOT NULL,
  `id_type` int(11) unsigned NOT NULL,
  `id_priority` int(11) unsigned NOT NULL,
  `id_unit` int(11) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `add_date` int(11) NOT NULL,
  `reason` text,
  PRIMARY KEY  (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_unit` (`id_unit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

$moduleInstall[] = "CREATE TABLE `notification_unit` (
  `id` int(11) NOT NULL auto_increment,
  `id_city` int(11) unsigned NOT NULL,
  `pos` int(6) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";


$moduleInstall[] = "CREATE TABLE `notification_city` (
  `id` int(11) NOT NULL auto_increment,
  `pos` int(6) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

$moduleInstall[] = "CREATE TABLE `notification_history` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_notification` int(11) unsigned NOT NULL,
  `id_user` int(11) unsigned NOT NULL,
  `id_status` int(11) unsigned NOT NULL,
  `id_priority` int(11) unsigned NOT NULL,
  `add_date` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_notification` (`id_notification`,`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

$moduleInstall[] = "CREATE TABLE `notification_priority` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `pos` int(6) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

$moduleInstall[] = "CREATE TABLE `notification_status` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `pos` int(6) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

$moduleInstall[] = "CREATE TABLE `notification_type` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(50) NOT NULL,
  `pos` int(6) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

$moduleInstall[] = "CREATE TABLE `notification_unit` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `id_city` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `pos` int(6) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id_city` (`id_city`,`name`,`poz`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";


$moduleInstall[] = "INSERT INTO `config` (`id`, `k`, `v`, `segment`) VALUES (NULL, 'email', '', 'notifications');";

					
$moduleUninstall = array();
$moduleUninstall[] = "DROP TABLE `notification`";
$moduleUninstall[] = "DROP TABLE `notification_city`";
$moduleUninstall[] = "DROP TABLE `notification_history`";
$moduleUninstall[] = "DROP TABLE `notification_priority`";
$moduleUninstall[] = "DROP TABLE `notification_status`";
$moduleUninstall[] = "DROP TABLE `notification_type`";
$moduleUninstall[] = "DROP TABLE `notification_unit`";




?>
