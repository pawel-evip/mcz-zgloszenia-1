<?php 
define('ENTRY_DIR_BIG_PHOTOS', 'public/entry/');
define('ENTRY_DIR_THUMBS', 'public/entry/thumbs/');
define('ENTRY_DIR_MINI', 'public/entry/mini/');
define('ENTRY_DIR_TMP', 'public/entry/tmp/');
define('ENTRY_THUMB_WIDTH', 330);
define('ENTRY_THUMB_HEIGHT', 215);
define('ENTRY_MWIDTH', 50);
define('ENTRY_MHEIGHT', 35);
define('ENTRY_DIR', 'public/entry/');
 
class Admin_EntryController extends Cube_Controller_Abstract
{
	private $_id;
	private $_cid;
	private $_js;
	
	private $_search = null;
	private $_order = null;	
	private $_sort_image_up=' <img src="templates/admin/images/up.png" alt="" />';
	private $_sort_image_down=' <img src="templates/admin/images/down.png" alt="" />';
		
	
	private function search_order()
	{
		$state = $this->_request->getParam('state',1);
		$column = $this->_request->getParam('column');	
		$search_string =$this->_request->getParam('search');
		
		//utworz obiekt Field(nazwa_pola,sql_pola)
		Cube_Loader::loadClass('Cube_SearchOrder_Field');
		$id = new Cube_SearchOrder_Field('id','d.id');
		$title = new Cube_SearchOrder_Field('title','d.title');
		$filename = new Cube_SearchOrder_Field('filename','d.filename');
		$date = new Cube_SearchOrder_Field('date','d.add_date');
		$size = new Cube_SearchOrder_Field('size','d.size');
		
		//utworz obiekt Search(szukane_slowo)
		Cube_Loader::loadClass('Cube_SearchOrder_Search');
		$search=$this->_search= $this->view->search= new Cube_SearchOrder_Search($search_string);
		//dodaj pola ktore bede przeszukiwane pod katem wystepowania slowa szukane_slowo
		$search->addField($id);
		$search->addField($title);
		$search->addField($filename);
		$search->addField($size);
		
		//utworz obiekt Order(($status,$field,$link,Cube_SearchOrder_Search $search=NULL)
		Cube_Loader::loadClass('Cube_SearchOrder_Order');
		
		$link='admin,entry';
		$image_up=$this->_sort_image_up;
		$image_down=$this->_sort_image_down;
		
		$order=$this->_order= $this->view->order=new Cube_SearchOrder_Order($state,$column,$link,$image_up,$image_down,$search);
		//dodaj pola ktore bede mogły być sortowane
		$order->addField($id);
		$order->addField($title);
		$order->addField($filename);
		$order->addField($date);
		$order->addField($size);
	}
	
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->view->username = $this->_session->getUsername();
		$this->_id = $this->_request->getParam('id', 0);
		$this->view->id = $this->_id;
		 $this->view->cid=$this->_cid = $this->_request->getParam('cid', 0);
		$this->view->jsTree = $this->displayJsTree2($this->_cid);
		//$this->_cid = $this->_request->getParam('cid', 0);
		//$this->view->cid = $this->_cid;	
		$this->doc_name=NULL;
		
		$this->search_order();
		
	}
	
	public function sortAction()
	{
		$model = new EntryDocs();
		$this->view->render('files');
		
		$where=$this->_order->createWhere();
		$field=$this->_order->getFieldSql();
		$order=$this->_order->getOrder();
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		
		if ($cid > 0) {
			$this->view->docs = $model->getAll('d.cid = "'.$cid.'" AND '.$where,$field.' '.$order);		
		} 
		else 
			$this->view->docs =$model->getAll($where,$field.' '.$order);
	}
	
	
	public function searchAction()
	{
		$search=clear($_POST['search']);
		$model = new EntryDocs();
		$this->view->render('files');
		$where=$this->_search->createWhere($search);
		
		//uwzgledniamy kategorie
		$cid=$this->_cid;
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		
		if ($cid > 0) {
			$this->view->docs = $model->getAll('d.cid = "'.$cid.'" AND '.$where, 'd.id DESC');		
		} 
		else 
			$this->view->docs = $model->getAll($where,'d.id DESC');
			
		if (sizeof($this->view->docs) < 1)
		{
			header('refresh: 3; url=admin,entry,files.html');
			$this->view->message = 'Dla pytania "'.$search.'" nie odnaleziono wyników w bazie.Przekierowywanie...';
		}
	}
	
	

	public function filesAction()
	{	
	
		$model = new EntryDocs();
		
		if ($this->_cid == 0) $cid = $this->view->cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		if ($cid > 0) {
			$this->view->docs = $model->getAll('d.cid = "'.$cid.'"', 'd.id DESC');	
			//$this->view->photos = $model->getPhotos(' WHERE cid = "'.$cid.'"');
			//$this->view->docs = $model->getDocs(' cid = "'.$cid.'"');	
		} else 	{
			$this->view->docs = $model->getAll(null, 'd.cid DESC, d.id DESC');
			//$this->view->photos = $model->getPhotos();
			//$this->view->docs = $model->getDocs();
			
		}
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($this->view->rows);
		//print("</pre><BR>");
		
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		//$this->view->categories = $cat = $model->getCats();

	}	
	
	public function adddocAction()
	{
	  	$this->view->render('files');
		if ($this->_request->isRedirected()) 
		{
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			
			$cid=$this->view->cid 	  = $params['cid'];
			$this->view->title   = $params['title'];	
			$this->view->description   = $params['description'];
			$this->view->categoriesList = $this->_getCategoriesList($cid);
			
			$model = new EntryDocs();
			if ($cid > 0) {
			$this->view->docs = $model->getAll('d.cid = "'.$cid.'"', 'd.id DESC');	
			//$this->view->photos = $model->getPhotos(' WHERE cid = "'.$cid.'"');
			//$this->view->docs = $model->getDocs(' cid = "'.$cid.'"');	
			} else 	{
			$this->view->docs = $model->getAll(null, 'd.cid DESC, d.id DESC');
			//$this->view->photos = $model->getPhotos();
			//$this->view->docs = $model->getDocs();
			
			}
			
			//$this->view->row 			= $params;						
			return;
		}	
		$this->view->categoriesList = $this->_getCategoriesList();
		$name = $this->_request->getParam('name', 0);
		//echo 'NAME='.$name;
		if ($name != null && is_string($name)) {
			$this->view->doc_name = $name;
			//echo 'NAME='.$this->view->doc_name;
		}
		
		if ($this->_request->isPost()) {
			$filter = new EntryFilter();
			$filter->filter();
			$data = $filter->getData();
			
			//print("Wynik  funkcji print_r:<BR><pre>");
			//print_r($data);
			//print("</pre><BR>");
			
			if ($_FILES['photo']['name'] != '' &&  $_FILES['photo']['tmp_name'] != '')
			{
				$tmp = $_FILES['photo']['tmp_name'];
				$name = $_FILES['photo']['name'];
				$size = $_FILES['photo']['size'];
				$type = $_FILES['photo']['type'];
			
				if(is_uploaded_file($tmp)) 
				{
					$doc_name = time().'_'.$name;
					move_uploaded_file($tmp, "public/entry/docs/$doc_name");
					$data['filename']  = $doc_name;
					//$data['typ'] = $type;
					
				} 
				else
				{
					{
						$this->_request->redirectFailure('Błąd podczas dodawania dokumentu! Przekierowywanie...');	
						return;
					}
				}
			}
			else
			{
				//jezeli dodajemy plik z serwera ftp (opcja nowe pliki).
				/*$path = $this->_categoriesModel->getPath($data['cid']);
				//echo 'PATCH='.$path;
				if ($path['path'] == '/')
					$path['path'] = '';
				*/	
				
				if ($data['movie'] != '')	{
				//echo 'MOVIE='.$data['movie'];
					$time_data_movie = time().'_'.$data['movie'];	
				  	if(file_exists('newmovie/'.$data['movie'])){
						$result = rename('newmovie/'.$data['movie'], 'public/entry/docs/'.$time_data_movie);
						$size=filesize('public/entry/docs/'.$time_data_movie);
						$data['filename']=$time_data_movie;
	    			} 
	    			if(!$result)
					{
	    				$this->_request->redirectFailure(array('Nie udało się dodać pliku.'));
						return;
					}
				}
			}		
			
			
			$data['size']  = $size;
			$model = new EntryDocs();
			$model->insertDocs($data);
			header('refresh: 3; url=admin,entry,redirect.html');
			$this->view->message = 'Dokument dodany pomyślnie! Przekierowywanie...';
			$this->view->render('index');
			//$this->view->render('index');
			//$this->view->message = 'Dokument dodany pomyślnie!';
			//header('Location: admin,entry,index.html');
			
		}    
	}
	
	public function newfilesAction()
	{
		$folder = "newmovie/*.doc";
		$files = array();
		$i = 0;
		if(! glob($folder, GLOB_BRACE) === false)
		{
			foreach(glob($folder, GLOB_BRACE) as $file){
				if($file != $folder && $file != '..'){
					$tmp = explode('/', $file);
					$files[$i]['id'] = $i;
					$files[$i]['filename'] = $tmp[1];
					$i++;
				}
			}
		}
		$folder = "newmovie/*.pdf";
		if(! glob($folder, GLOB_BRACE) === false)
		{
			foreach(glob($folder, GLOB_BRACE) as $file){
				if($file != $folder && $file != '..'){
					$tmp = explode('/', $file);
					$files[$i]['id'] = $i;
					$files[$i]['filename'] = $tmp[1];
					$i++;
				}
			}
		}
		$name = $this->_request->getParam('name', 0);
		if ($name != null && is_string($name)) {
			$this->view->movie = $name;
		}
		
		$this->view->rows = $files;		
		
	}
	/*public function deletephotoAction(){
	  $this->view->render('index'); 
		$model = new Entry();
		$model->deletePhoto($this->_id);
		header('refresh: 3; url=admin,entry,redirect.html');
		$this->view->message = 'Zdjęcie usunięte pomyślnie ! Przekierowywanie...';
		$this->view->render('index');

 	}*/
	
	public function deletedocAction(){

	  $this->view->render('index'); 
		$model = new EntryDocs();
		$model->deleteDocs($this->_id);
		header('refresh: 3; url=admin,entry,redirect.html');
		$this->view->message = 'Dokument usunięty pomyślnie ! Przekierowywanie...';
		
	}	

	public function deletefilesAction()
	{
	  	//usuwanie plikow w opcji newfiles
		$this->view->render('index');
		$name = $this->_request->getParam('name', 0);
		if ($name != null && is_string($name)) {
		
			if(file_exists('newmovie/'.$name)){
				unlink('newmovie/'.$name);
			}
			header('refresh: 3; url=admin,entry,redirect.html');
			$this->view->message = 'Plik usunięty pomyślnie ! Przekierowywanie...';
			$this->view->render('newfiles');
		}
	}
	
	/*public function addmovieAction()
	{
		//$this->view->render('index');
				
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->category 	  = $params['cid'];
			$this->view->title 	  = $params['title'];
			$this->view->description = $params['description'];	
			$this->view->movie 	  = $params['movie'];
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);				
			return;
		}
		
		$name = $this->_request->getParam('name', 0);
		if ($name != null && is_string($name)) {
			$this->view->movie = $name;
		}
			
		if ($this->_request->isPost()) {
			
			$filter = new EntryFilesFilter();
			$filter->filter();	
			$data = $filter->getData();

				$filter = new EntryFilter();
				$filter->filter();	
				$data = $filter->getData();
				//$data['movie'] = $_POST['movie'];
				$this->view->categoriesList = $this->_getCategoriesList($data['cid']);
				
				
				$path = $this->_categoriesModel->getPath($data['cid']);
				
				if ($path['path'] == '/')
					$path['path'] = '';
					
				
				if ($data['movie'] != '')	{ 
				  	$data['movie'] = time().'_'.$data['movie'];
	    			if(file_exists('newmovie/'.$data['movie'])){ 
						$result = rename('newmovie/'.$data['movie'], 'public/entry/movie/'.$data['movie']);
	    			} 
	    			if(!$result)
	    				$this->_request->redirectFailure(array('Nie udało się dodać pliku.'));
				} 
				$data['filename'] = $data['movie'];
				unset($data['movie']);
				$model = new Entry();
				$model->insert($data);	
				header('refresh: 3; url=admin,entry,redirect.html');
				$this->view->message = 'Wpis dodany pomyślnie ! Przekierowywanie...';	
		}	
	}*/
	
	public function redirectAction()
	{
		header('Location: admin,entry.html');
	}
	
	public function redirectsettAction()
	{
		header('Location: admin,entry,settings.html');
	}	
	
	/*public function editAction() 
	{
		$model = new Entry();
		$this->view->row = $row = $model->get($this->_id);
		//echo $this->_id;
		//print_R($row);
		//exit;
		$this->view->id 	  = $row['id'];
		$this->view->category 	  = $row['cid'];
		$this->view->categoriesList = $this->_getCategoriesList($row['cid']);
		$this->view->title 	  = $row['title'];
		$this->view->description = $row['description'];
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->category 	  = $params['cid'];
			$this->view->title 	  = $params['title'];
			$this->view->description = $params['description'];	
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);							
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new EntryFilter();
			$filter->filter();
			$data = $filter->getData();
			unset($data['movie']);	
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,entry.html');
			$this->view->message = 'Wpis zaktualizowany pomyślnie ! Przekierowywanie...';	
		}
	}*/	
	
	/*public function editphotoAction() 
	{
		$model = new Entry();
		$row = $model->getPhotos('WHERE id = '.$this->_id);
		$this->view->row = $row[0];
		$row = $row[0];
		//echo $this->_id;
		//print_R($row);
		//exit;
		$this->view->id 	  = $row['id'];
		$this->view->category 	  = $row['cid'];
		$this->view->categoriesList = $this->_getCategoriesList($row['cid']);
		$this->view->title 	  = $row['title'];
		$this->view->description = $row['description'];
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->category 	  = $params['cid'];
			$this->view->title 	  = $params['title'];
			$this->view->description = $params['description'];	
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);							
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new EntryFilter();
			$filter->filter();
			$data = $filter->getData();
			unset($data['movie']);	
			$model->updatePhoto($this->_id, $data);
			header('refresh: 3; url=admin,entry.html');
			$this->view->message = 'Wpis zaktualizowany pomyślnie ! Przekierowywanie...';	
		}
	}*/
	
	public function editdocAction() 
	{
		$model = new EntryDocs();
		$row = $model->get($this->_id);
		//print_r($row);
		$this->view->row = $row; 
		//echo $this->_id;
		//print_R($row);
		//exit;
		$this->view->id 	  = $row['id'];
		$this->view->category 	  = $row['cid'];
		$this->view->categoriesList = $this->_getCategoriesList($row['cid']);
		$this->view->title 	  = $row['title'];
		$this->view->description = $row['description'];
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->category 	  = $params['cid'];
			$this->view->title 	  = $params['title'];
			$this->view->description = $params['description'];	
			$this->view->categoriesList = $this->_getCategoriesList($params['cid']);							
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new EntryFilter();
			$filter->filter();
			$data = $filter->getData();
			unset($data['movie']);	
			//$model->updateDoc($this->_id, $data);
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,entry.html');
			$this->view->message = 'Wpis zaktualizowany pomyślnie ! Przekierowywanie...';	
		}
	}	
	
	/*public function deleteAction()
	{
	  	$this->view->render('index');
		$model = new Entry();
		/*
		$row = $model->get($this->_id);	
		if($row['filename'] != '' && $row['filename'] != null )
			if (file_exists(ENTRY_DIR.$row['filename']))
				unlink(ENTRY_DIR.$row['filename']);
				
		$model->delete($this->_id);
		header('refresh: 3; url=admin,entry,index.html');
		$this->view->message = 'Wpis usunięty pomyślnie ! Przekierowywanie...';
		$this->view->render('index');
	}*/	
	
	// =====================
	public function indexAction()
	{
		
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->cid=$cid;
		//$this->view->jsTree = $this->displayJsTree2($cid);
		//echo 'CID='.$cid;
		//echo 'JS='.$this->_js;
		$this->view->rows = $this->_getSubCategories($cid);
		//$model = new EntryTree();
		//$this->view->rows = $model->getAllChildren($cid);
		//$cats =  $this->_getCategories();
		//$this->view->currentPath = $cats[$cid];
		$this->view->currentPath = $this->_getBranchPatch($cid);
		//$this->view->current = $this->_categoriesModel->get($cid);
		
		//Dokumenty
		$model = new EntryDocs();
		if ($cid > 0) {
			$this->view->docs = $model->getAll('d.cid = "'.$cid.'"', 'd.id DESC');	
			//$this->view->photos = $model->getPhotos(' WHERE cid = "'.$cid.'"');
			//$this->view->docs = $model->getDocs(' cid = "'.$cid.'"');	
		} else 	{
			$this->view->docs = $model->getAll(null, 'd.cid DESC, d.id DESC');
			//$this->view->photos = $model->getPhotos();
			//$this->view->docs = $model->getDocs();
			
		}
		$this->view->categoriesList = $this->_getCategoriesList($cid);
	}

	private function displayJsTree2($current)
	{
		$conts = "<a href=\"javascript: d.openAll();\" class=\"close\">+ rozwiń drzewko</a><a href=\"javascript: d.closeAll();\" class=\"close\">- zwiń drzewko</a><br clear=\"all\" /><br />
			<div class=\"dtree\">
			<noscript>
				Twoja przegladarka nie obsługuje javascript!
			</noscript>
			<script type=\"text/javascript\">
		<!--

		d = new dTree('d');";
		
		$model = new EntryTree();
		$rows=$model->getAllTreeWSTCC(null,'t.name');
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		//$rows = $this->getDepth();
		//$rows = array_msort($rows, array( 'depth' => array( SORT_ASC), 'pos' => array( SORT_ASC)));
		
		if(count($rows) > 0)
		{
			
			//$was = false;
			foreach($rows as $row)
			{
				//print_r($row);
				// czysc stos
				/*if(count($right) > 0)
				{		
					$was = true;			
					while($right[count($right)-1] < $row[$this->_right])
					{
						array_pop($right);
					}			
				}
				
				$items = ($row[$this->_right]-$row[$this->_left]-1)/2;
				*/
				if( !isSet($row['count_child']) )
					$row['count_child']=0;
					
				//$link = HTML_BASE.'admin/shop/product/index/cid_'.$row['id'].'/';
				$link='admin,entry,index,cid_'.$row['id'].'.html';
				require 'templates/admin/tree/show_tree.php';
				
				/*if ($row['parent_id'] == 0) $row['parent_id'] = -1;
				
				if ($row['id'] == $current) {
					$conts .= "
					d.add(".$row['id'].",".$row['parent_id'].",'<b class=\"green\">".$row['name']." (".$row['count_child'].")</b>', '".$link."', '".$row['name']."', '', 'templates/admin/tree/img/folder.gif');";				
				} else if ($row['active']== 0){
					//categoria nieaktywna
					$conts .= "
					d.add(".$row['id'].",".$row['parent_id'].",'<b class=\"red\">".$row['name']." (".$row['count_child'].")</b>', '".$link."', '".$row['name']."', '', 'templates/admin/tree/img/folder.gif');";				
				} else {
					$conts .= "
					d.add(".$row['id'].",".$row['parent_id'].",'<b class=\"no-bold\">".$row['name']." (".$row['count_child'].")</b>', '".$link."', '".$row['name']."', '', 'templates/admin/tree/img/folder.gif');";				
				}*/
				
				
				//$right[] = $row[$this->_right];
			}
							
			//if (!$was) {
			//	$conts = '<p>brak kategorii</p>';
			//} else 
			{
				$conts .= '
				d.openTo('.$current.', true);
				document.write(d);

				//-->
				</script>
				</div>';
			}	
		} else $conts = '<p>brak kategorii</p>';
		
		return $conts;	
	}
	
	
	
	public function poscategoriesAction()
	{
		$this->view->render('index');
		$model = new EntryTree();
		$rows=$model->getAllChildren($this->_cid);	
		//echo 'CID='.$this->_cid;
		//exit;
		foreach ($rows as $r)
		{
			$data['pos'] = (int)clear($_POST['pos_'.$r['id']]);	
			$model->update($r['id'], $data);	
		}
		header('refresh: 3; url=admin,entry,index,cid_'.$this->_cid.'.html');
		$this->view->message = 'Kategoria zaktualizowana pomyślnie ! Przekierowywanie...';
	}	
	
	
	public function addcatAction()
	{
		$this->view->name = null;
		if ($this->_cid == 0) $cid = clear($this->_request->getPost('cid', 0));
		else $cid = $this->_cid;
		
		$this->view->categoriesList = $this->_getCategoriesList($cid);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$cid=$this->view->cid 	  = $params['cid'];
			$this->view->name   = $params['name'];	
			$this->view->description   = $params['description'];
			$this->view->currentPath = $this->_getBranchPatch($cid);
			$this->view->categoriesList = $this->_getCategoriesList($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
			//print_r($params);
			//print("</pre><BR>");
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new EntryCategoryFilter();
			$filter->filter();			
			$model = new EntryTree();
			$data = $filter->getData();
			
			//$parent = $this->_categoriesModel->get($data['pid']);
			//$data['path'] = profileName($parent['path'].$data['name'].'/');
			$data['parent_id'] = $data['cid'];
			unset($data['cid']);
			$model->insertTree($data);
			
		//	if (!file_exists(ENTRY_DIR.$data['path']))
		//		mkdir(ENTRY_DIR.$data['path'], 0777);
											
			//$model->insert($data);	
			header('refresh: 3; url=admin,entry,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria dodana pomyślnie ! Przekierowywanie...';
		}			
	}	
	
	public function editcatAction()
	{
		$model = new EntryTree();
		//echo 'ID='.$this->_id;
		$row = $model->get($this->_id);
		//print_r($row);
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		$this->view->description   = $row['description'];	
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];	
			$this->view->description   = $params['description'];
					
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new EntryEditCategoryFilter();
			$filter->filter();
			$data = $filter->getData();
			$model->update($this->_id, $data);
			header('refresh: 3; url=admin,entry,index,cid_'.$this->_cid.'.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie ! Przekierowywanie...';	
		}
	}
	/////
	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList($this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList($params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) 
		{
			$filter = new EntryDeleteCategoryFilter();
			$filter->filter();	
			$data = $filter->getData();
			//$act = clear($_POST['act']);
			$act=$data['act'];
			$cat=$data['cat'];
			$class=array('EntryDocs');	//Tabela z nazwami tabel zaiazanych z drzewem z ktorych trzeba usunac np. dokumenty, zdjecia itp
			if ($act == 'delete') {
				//$model = new MediaDocs();
				//$model->deleteAll($this->_id);
				
				$model = new EntryTree();
				$model->deleteTree($this->_id,$class);	
				//$model->deleteAllPhotos($this->_id);
				//$model->deleteAll($this->_id);
				header('refresh: 3; url=admin,entry,index.html');
				$this->view->message = 'Kategoria oraz dokumenty usunięte pomyślnie ! Przekierowywanie...';
				$this->view->render('index');		
			} 
			
			else if ($act == 'przenies_doc')  {
				//$cat = clear($_POST['cat']);
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść dokumenty.'));	
				$model = new EntryTree();
				$model->deleteTree($this->_id,$class,$cat,false);
				//$model = new EntryDocs();
				//$model->setCategories($this->_id, $cat);
				header('refresh: 3; url=admin,entry,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie ! Dokumenty przeniesione ! Przekierowywanie...';
				$this->view->render('index');					
			} 
			
			else if ($act == 'przenies_cat')  {
				//$cat = clear($_POST['cat']);
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść dokumenty i podkategorie.'));	
				$model = new EntryTree();
				$model->moveAllBranchTree($this->_id,$cat);
				$model = new EntryTree();
				$model->deleteTree($this->_id,$class,$cat,false);
				header('refresh: 3; url=admin,entry,index.html');
				$this->view->message = 'Kategoria usunięta pomyślnie ! Dokumenty przeniesione z usunietej kategorii przeniesione. Podkategorie przeniesione ! Przekierowywanie...';
				$this->view->render('index');
			}
		}
	}	
	
	public function deactivecatAction()
	{
		$model = new EntryTree();
		$model->deactiveSubTree($this->_id);
		header('refresh: 3; url=admin,entry,index.html');
		$this->view->message = 'Kategoria iwszystkie podkategorie wyłączone pomyślnie ! Przekierowywanie...';
		$this->view->render('index');
	}
		
	public function activecatAction()
	{
		$model = new EntryTree();
		$model->activeSubTree($this->_id);
		header('refresh: 3; url=admin,entry,index.html');
		$this->view->message = 'Kategoria iwszystkie podkategorie włączone pomyślnie ! Przekierowywanie...';
		$this->view->render('index');
	}	
	
	public function settingsAction()
	{
		$segment = $this->_config->segment('entry');
		
		foreach ($segment as $s)
		{
			$this->view->row[$s['k']] = $s['v'];
		}
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			return;
		}
		if ($this->_request->isPost()) {		
			$pp = (int)clear($_POST['per_page']);
			
			if ($pp == '') $this->_request->redirectFailure(array('Pole "Zdjęć na stronę" jest wymagane.'));
								
			$this->_config->update('per_page', $pp, 'entry');

			header('refresh: 3; url=admin,entry,settings.html');
			$this->view->message = 'Ustawienia zmienione pomyślnie ! Przekierowywanie...';
		}			
	}
	
	
	# zwraca tablice postaci: id => sciezka typu Top/Test/Subtest, gdzie ID to nr id kategorii subtest
	/*private function _getTree($parent_id, $current_name, &$tree)
	{	
		$subcats = $this->_categoriesModel->getSubcategories($parent_id);
		if (count($subcats) < 1)
			return $tree;
			
		foreach ($subcats as $sub)
		{
			$tree[$sub['id']] = $current_name.$sub['name'].'/';
			$this->_getTree($sub['id'], $tree[$sub['id']], $tree);	
		}	
		
		return $tree;
	}*/

	/*public function _getCategories()
	{
		$tree = array(1 => 'Korzeń/');
		$tree = (array)$this->_getTree(1, 'Korzeń/', $tree);
		asort($tree);
		
		//print_r($tree);
		return $tree;	
	}*/
	
	public function _getBranchPatch($cid)
	{
	  	//zwraca sciezke do galezi (od dziecka do ostatniego rodzica (Korzenia))
	  	$model = new EntryTree();
		$rows=$model->getBranchTree($cid);
		return $rows;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		/*$temp='';
		foreach($rows as $row)
			$temp.=$row['name'].'/';
		
		return $temp;*/
		
	}

	//public function _getCategoriesList($cid = null, $no = false)
	public function _getCategoriesList($cid = null, $no = false)
	{
		$model = new EntryTree();
		if (!$no)
			$rows=$model->getAllTreePath();
		else
		{
			//jesli delete
			$rows=$model->getAllTreePath($cid);
			//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		}	
		$temp='';
		$path='';
		
		//usuń i pobierz pierwszy element tablicy - KORZEŃ
		$row=array_shift($rows);
		if (!is_null($cid) && $row['id'] == $cid) 
		{
			if (!$no) 
				$temp .= '<option value="'.$row['id'].'" selected="selected">'.$row['name'].'</option>';
		} 
		else 
			$temp .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
		
		
		if(count($rows > 0))
		{
			//dodaj korzeń na koniec, potrzebne do rozpoznania ostatniej gałęzi.
			$rows[]=$rows[0];
			//usun korzeń z początku, przeszkadza w rozpoznaniu pierwszej gałęzi.
			array_shift($rows); 
			foreach($rows as $row)
			{
				if($row['id'] == 1)
				{
					$path=$row['name'].'/'.$path;
					if (!is_null($cid) && $last_id == $cid)
					{
						if (!$no) 
							$temp .= '<option value="'.$last_id.'" selected="selected">'.$path.'</option>';
					}
					else 
						$temp .= '<option value="'.$last_id.'">'.$path.'</option>';
					
					$path='';
						
				}
				else
				{
					$path.=$row['name'].'/';
					$last_id=$row['id'];	
				
				}	
			}
		}

		//echo 'TEMP='.$temp;
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		return $temp;
		//ORYGINAL
		/*$tree = array(1 => 'Korzeń/');
		$tree = (array)$this->_getTree(1, 'Korzeń/', $tree);
		asort($tree);
		
		$temp = '';
		foreach ($tree as $id => $name)
		{
			if (!is_null($cid) && $id == $cid) {
				if (!$no) $temp .= '<option value="'.$id.'" selected="selected">'.$name.'</option>';
			} else $temp .= '<option value="'.$id.'">'.$name.'</option>';
		}
		return $temp;*/	
	}

	protected function _getSubCategories($parent_id)
	{
		$model = new EntryTree();
		$rows=$model->getAllChildren($parent_id);
		
		//ile dokumentow jest w kazdej kategorii
		$model = new EntryDocs();
		foreach ($rows as $i=>$row)
		{
			$temp=$model->count_rows('cid='.$row['id']);	
			$rows[$i]['doc_amount']=$temp['amount'];
		}
		//print("Wynik  funkcji print_r:<BR><pre>");
		//print_r($rows);
		//print("</pre><BR>");
		
		return $rows;
		//oryginalny
		//$cats = $this->_categoriesModel->getSubcategories($parent_id);
		//return $cats;
	}
	
	public function _image2($name, $saveas = null)
	{
		$pathTemp = 'public/entry/tmp/';
		$pathBig  = 'public/entry/';
		$pathThumb= 'public/entry/thumbs/';
		$pathMini= 'public/entry/mini/';
				
		$image = new Cube_Upload_Image('photo');
		$image->setPath($pathTemp);
		$image->setMaxSize(10240);
		
		if ($saveas != null) $image->setSaveAs(':id_:name', array(':id' => $saveas));
		else $image->setSaveAs(':name');
			
		$image->move();
				
		if ($image->errorExists()) $this->_request->redirectFailure($image->getErrors());	
		$filename = $image->getSaveName();
		if (file_exists($pathBig.$filename)) $this->_request->redirectFailure(array('Już istnieje w bazie zdjęcie o takiej nazwie pliku ('.$filename.').'));
		if ($image->getX() > 800 || $image->getY() > 600) {
		if (!$image->resize(800, 600, $pathBig)) $this->_request->redirectFailure(array('Nie udało się zmniejszyć zdjęcia do rozmiarów 800x600.'));
		} else copy($pathTemp.$filename, $pathBig.$filename);
		if ($image->getX() > 120|| $image->getY() > 90) {
			if (!$image->resize(120,90, $pathThumb)) $this->_request->redirectFailure(array('Nie udało się utworzyć miniaturki.'));
		} else copy($pathTemp.$filename, $pathThumb.$filename); 	
		if ($image->getX() > 50 || $image->getY() > 60) {
			if (!$image->resize(50, 60, $pathMini)) $this->_request->redirectFailure(array('Nie udało się utworzyć miniaturki.'));
		} else copy($pathTemp.$filename, $pathMini.$filename); 				
		// usuwa tymczasowe zdjecie
		$image->deleteImage();
		return $filename;			
	}	
	
}

?>
