<?php 

class Admin_MenusController extends Cube_Controller_Abstract
{
	private $_id = null;
	private $_mid = null;
		
	public function init()
	{
		$this->view->setTemplate('admin');
		$this->view->mid = $this->_mid = $this->_request->getParam('mid', 0);	
		$this->view->id = $this->_id = $this->_request->getParam('id', 0);	
		$this->view->render('index');					   
	}
		
	private function _info($id, $error = false)
	{
		$this->view->error = false;
		if ($error) $this->view->error = true;
	
		$infos = array(0 => 'Nieprawidłowe żądanie',
		'Pole "Język" jest wymagane.',
		'Pole "Nazwa" jest wymagane.',
		'Grupa menu dodana pomyślnie',
		'Pole "Odnośnik" jest wymagane.',
		'Pole "Numer artykułu/Adres" jest wymagane.',
		'Menu dodane pomyślnie',		
		'Grupa menu zaktualizowana pomyślnie',
		'Menu zaktualizowane pomyślnie',
		'Grupa oraz menu należące do niej usunięte pomyślnie',
		'Menu usunięte pomyślnie',
		'Grupa menu włączona pomyślnie',
		'Menu włączone pomyślnie',
		'Grupa menu wyłączona pomyślnie',
		'Menu wyłączone pomyślnie');
		header('refresh:3;url=admin,menus.html');
		$this->view->render('info');
		$this->view->message = $infos[$id].'! Przekierowywanie...';
	}	
	
	public function indexAction()
	{	
		$model = new Menus();
		$this->view->menus = $model->getMenus();
		$this->view->rows = $model->getMenusRows();	
	}	
	
	public function addmenuAction()
	{
		$data['language'] = clear($_POST['language']);
		$data['name'] = clear($_POST['name']);
		$data['logged'] = clear($_POST['logged']);
		
		if ($data['language'] == '') {
			$this->_info(1, true);			
			return;
		}
		if ($data['name'] == '') {
			$this->_info(2, true);			
			return;
		}
		
		$model = new Menus();
		$model->insertMenu($data);
		$this->_info(3);		
	}
	
	public function addrowAction()
	{
		$data['label'] = clear($_POST['label']);
		$data['href'] = clear($_POST['href']);
		$data['mid'] = clear($_POST['mid']);	
		if ($data['label'] == '') {
			$this->_info(4, true);			
			return;
		}
		if ($data['href'] == '') {
			$this->_info(5, true);			
			return;
		}
		
		$model = new Menus();
		$model->insertRow($data);
		$this->_info(6);	
	}
	
	public function updateAction()
	{
		if ($this->_mid > 0) {
			$data['language'] = clear($_POST['language']);
			$data['name'] = clear($_POST['name']);
			$data['logged'] = clear($_POST['logged']);
			
			if ($data['language'] == '') {
				$this->_info(1, true);			
				return;
			}
			if ($data['name'] == '') {
				$this->_info(2, true);			
				return;
			}
			
			$model = new Menus();
			$model->updateMenu($this->_mid, $data);
			$this->_info(7);
		} else {
			if (!$this->_id) {
				$this->_info(0, true);			
				return;
			}
			$data['label'] = clear($_POST['label']);
			$data['href'] = clear($_POST['href']);
			
			if ($data['label'] == '') {
				$this->_info(4, true);			
				return;
			}
			if ($data['href'] == '') {
				$this->_info(5, true);			
				return;
			}
			
			$model = new Menus();
			$model->updateRow($this->_id, $data);
			$this->_info(8);
		}
	}	
	
	public function deleteAction()
	{
		if ($this->_mid > 0) {
			$model = new Menus();
			$model->deleteMenu($this->_mid);
			$this->_info(9);	
		} else {
			if (!$this->_id) {
				$this->_info(0, true);			
				return;
			}
			$model = new Menus();
			$model->deleteRow($this->_id);
			$this->_info(10);
		}
	}	
	
	public function activeAction()
	{
		if ($this->_mid > 0) {
			$model = new Menus();
			$model->activeMenu($this->_mid);
			$this->_info(11);
		} else $this->_info(0);
	}
	
	public function deactiveAction()
	{
		if ($this->_mid > 0) {
			$model = new Menus();
			$model->deactiveMenu($this->_mid);
			$this->_info(13);
		} else $this->_info(0);
	}	
}

?>
