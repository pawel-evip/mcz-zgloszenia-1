<?php 

class Admin_AdvnewsController extends Cube_Controller_Abstract
{
	private $_id;
	
	private $_id_state=null;
	private $_title_state=null;
	private $_adddate_state=null;
	private $_author_state=null;
	private $_category_state=null;
	private $_search=null;
	
	
	
	private function create_where($search)
	{
		$where_title='n.id like "%'.$search.'%"';
		$where_id='n.title like "%'.$search.'%"';
		$where='('.$where_title.' OR '.$where_id.')';
		return $where;
		
	}
	
	private function sort($field,$sort)
	//public function sortLoginAction()
	{
		
		$model = new AdvNews();
		//$field='Login';
		
		
		if ($sort==1)
			$order='ASC';
		elseif ($sort==2)
			$order='DESC';
	
	
		$where=$this->create_where($this->_search);
		
		$this->view->rows = $model->getAll($where,''.$field.' '.$order.'');
		//sort($order, $field);
		$this->view->render('index');
		
	}
	
	public function init()
	{
		$this->view->setTemplate('admin');		
		$this->view->id = $this->_id = $this->_request->getParam('id', 0);	
		
		$this->view->id_state = $this->_id_state = $this->_request->getParam('idstate');
		$this->view->title_state = $this->_title_state = $this->_request->getParam('titlestate');	
		$this->view->adddate_state = $this->_adddate_state = $this->_request->getParam('adddatestate');
		$this->view->author_state = $this->_author_state = $this->_request->getParam('authorstate');	
		$this->view->category_state = $this->_id_category = $this->_request->getParam('categorystate');
		$this->view->search = $this->_search = $this->_request->getParam('search');	
		
		
		
	}

	public function indexAction()
	{			
		$model = new AdvNews();
		$this->view->rows = $model->getAll(null, 'n.id DESC');
	}
	
	public function searchAction()
	{
		$search=$_POST['search'];
		//echo 'search ='. $search;
		$model = new AdvNews();
		//$where='p.title like "%'.$search.'%"';
		
		$where=$this->create_where($search);
		$this->view->rows = $model->getAll($where,null);
		if (sizeof($this->view->rows) < 1)
		{
			//$this->_request->redirectFailure(array('News musi przynależeć co najmniej do jednej kategorii !'));
			//echo "Jestem w IF";
			header('refresh: 3; url=admin,advnews.html');
			$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
			$this->view->render('index');
			
								
			//$this->view->message = 'Dla pytania '.$search.' nie odnaleziono wyników w bazie.Przekierowywanie...';
			//$this->view->render('order');
			//$this->redirect(3, null, 'index');	
			return;
		}
		$this->view->render('index');
	}
		
	public function sortCategoryAction()
	{
		$field='cs.name';
		$sort=$this->_title_state;
		$this->sort($field,$sort);		
	}
	
	
	public function sortAuthorAction()
	{
		$field='n.author';
		$sort=$this->_author_state;
		$this->sort($field,$sort);		
	}
	
	public function sortAddDateAction()
	{
		$field='n.add_date';
		$sort=$this->_adddate_state;
		$this->sort($field,$sort);		
	}
		
	
	public function sortTitleAction()
	{
		$field='n.title';
		$sort=$this->_title_state;
		$this->sort($field,$sort);		
	}
	
	public function sortIDAction()
	{
		$field='n.id';
		$sort=$this->_id_state;
		$this->sort($field,$sort);		
	}
	
	private function _setExistsFiles()
	{
		$files = array();
		$dir_name = ADVNEWS_DIR_BIG_PHOTOS;	
		$dir_handle = dir($dir_name);
		while (FALSE !== ($fileData = $dir_handle->read()))
		{
			if (is_file($dir_name . $fileData)) $files[] = $fileData;
		}
		$dir_handle->rewind();	
		$dir_handle->close();
		$this->view->files = $files;
	}
	
	public function insertAction()
	{		
		$this->view->row['author'] = $this->_session->getUsername();
		$this->view->cats = $this->_getCategories();
		$this->_setExistsFiles();
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new AdvNewsFilter();
			$filter->filter();	
			$data = $filter->getData();
			$data['filename'] = '';
			
			if ($_FILES['photo']['name'] != '' &&  $_FILES['photo']['tmp_name'] != '')	{
				$pathBig = ADVNEWS_DIR_BIG_PHOTOS;
				$pathThumb= ADVNEWS_DIR_THUMBS; 	
						
				Cube_Loader::loadClass('Cube_Upload_Image');
				$image = new Cube_Upload_Image('photo');
				$image->setPath($pathBig);
				$image->setMaxSize(10240);
				$image->setSaveAs(':name');	
				$image->move();
				
				if ($image->errorExists()) $this->_request->redirectFailure($image->getErrors());	
				$filename = $image->getSaveName();
				if (file_exists($pathThumb.$filename)) $this->_request->redirectFailure(array('Już istnieje w bazie zdjęcie o takiej nazwie pliku ('.$filename.').'));
				if (!$image->resize(ADVNEWS_THUMB_WIDTH, ADVNEWS_THUMB_HEIGHT, $pathThumb, true)) $this->_request->redirectFailure(array('Nie udało się zmniejszyć zdjęcia do rozmiarów '.ADVNEWS_THUMB_WIDTH.'x'.ADVNEWS_THUMB_HEIGHT.'.'));
				
				$data['filename'] = $filename;					
			} else {
				if ($data['exists_filename'] != '') $data['filename'] = $data['exists_filename'];
			}
			
			$cat_values = (array)$_POST['cat_value'];
			$cat_ids  = (array)$_POST['cat_id'];
			$test = 0;
			foreach ($cat_values as $k => $v) {
				if ($v == 1) 
					$test++;
			}
			
			if ($test == 0)
				$this->_request->redirectFailure(array('News musi przynależeć co najmniej do jednej kategorii !'));
			 		
			$model = new AdvNews();
			$id = $model->insert($data);
			
			#kategorie
			foreach ($cat_values as $k => $v) {
				if ($v == 1) 
					$model->appendCat($id['id'], $cat_ids[$k]);
			}
			
			# wysyła maila dla newsinfo
			if (file_exists('system/models/Newsinfo.php')) {
				$newsinfo = new Newsinfo();
				$newsinfo->send($data);	
			}
				
			header('refresh: 3; url=admin,advnews.html');
			$this->view->message = 'News dodany pomyślnie ! Przekierowywanie...';
		}
	}
	
	public function editAction()
	{
		$model = new AdvNews();
		$this->view->row = $row = $model->get($this->_id);
		$this->view->cats = $this->_getCategories();
		$this->_setExistsFiles();
		$this->view->row['exists_filename'] = $this->view->row['filename'];
		
		$this->view->catsList = $this->_getCatsList($this->_id);
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new AdvNewsFilter();
			$filter->filter();
			
			
			$cat_values = (array)$_POST['cat_value'];
			$cat_ids  = (array)$_POST['cat_id'];
			$test = 0;
			foreach ($cat_values as $k => $v) {
				$v = explode('_',$v);
				if ($v[0] == 1) 
					$test++;
			}
			
			$data = $filter->getData();
			if ($test == 0)
				$this->_request->redirectFailure(array('News musi przynależeć co najmniej do jednej kategorii !'));
			
			if ($data['exists_filename'] != '') $data['filename'] = $data['exists_filename'];
		
			$model->update($this->_id, $data);
			
			#kategorie

			foreach ($cat_ids as $k => $v) {
				$find = false;
				foreach ($cat_values as $b)
				{
					if ($b == '1_'.$v) {
						$model->appendCat($this->_id, $v);
						$find = true;
						break;
					}	
				}
				if (!$find)
					$model->deleteCat($this->_id, $v);	
			}			
			
			header('refresh: 3; url=admin,advnews.html');
			$this->view->message = 'News zaktualizowany pomyślnie ! Przekierowywanie...';	
		}
	}
	
	# CATEGORIES
	protected function _getCategories()
	{
		$model = new AdvNews();
		$cats = $model->getCategories(null, 'name ASC');
		return $cats;
	}
	
	protected function _getCatsList($nid)
	{
		$model = new AdvNews();
		$cats = $model->getCatsList($nid);
		return $cats;
	}	
	
	protected function _getCategoriesList($cid = null, $no = false)
	{
		$cats = $this->_getCategories();
		$temp = '';
		foreach ($cats as $cat)
		{
			if (!is_null($cid) && $cat['id'] == $cid) {
				if (!$no) $temp .= '<option value="'.$cat['id'].'" selected="selected">'.$cat['name'].'</option>';
			} else $temp .= '<option value="'.$cat['id'].'">'.$cat['name'].'</option>';
		}
		return $temp;
	}	
	
	public function categoriesAction()
	{
		$this->view->rows = $this->_getCategories();
	}
	
	public function addcatAction()
	{
		$this->view->name = null;
		$this->view->render('categories');		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->name   = $params['name'];		
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new AdvNewsCategoryFilter();
			$filter->filter();			
			$model = new AdvNews();
			$data=$filter->getData();
			$model->addCategory($data);	
			header('refresh: 3; url=admin,advnews,categories.html');
			$this->view->message = 'Kategoria dodana pomyślnie ! Przekierowywanie...';
		}			
	}	
	
	public function editcatAction()
	{
		$model = new AdvNews();
		$row = $model->getCategory($this->_id);
		$this->view->id 	  = $row['id'];
		$this->view->name   = $row['name'];
		$this->view->max_chars_main 	  = $row['max_chars_main'];
		$this->view->max_chars_advnews   = $row['max_chars_advnews'];
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->name   = $params['name'];	
			$this->view->max_chars_main 	  = $row['max_chars_main'];
			$this->view->max_chars_advnews   = $row['max_chars_advnews'];	
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new AdvNewsCategoryFilter();
			$filter->filter();
			$model->updateCategory($this->_id, $filter->getData());
			header('refresh: 3; url=admin,advnews,categories.html');
			$this->view->message = 'Kategoria zaktualizowana pomyślnie ! Przekierowywanie...';	
		}
	}

	public function deletecatAction()
	{
		$this->view->id = $this->_id;
		$this->view->categoriesList = $this->_getCategoriesList($this->_id, true);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$params = $this->_request->getParamsFromLastRequest(); 
			$this->view->id 	  = $params['id'];
			$this->view->act   = $params['act'];
			$this->view->cat   = $params['cat'];
			$this->view->categoriesList = $this->_getCategoriesList($params['id'], true);			
			return;
		}			
		
		if ($this->_request->isPost()) {
			$act = clear($_POST['act']);	
			if ($act == 'delete') {
				$model = new AdvNews();
				$model->deleteCategory($this->_id);
				$model->deleteAllNews($this->_id);
				header('refresh: 3; url=admin,advnews,categories.html');
				$this->view->message = 'Kategoria oraz newsy usunięte pomyślnie ! Przekierowywanie...';
				$this->view->render('categories');		
			} else {
				$cat = clear($_POST['cat']);
				if ($cat == '0' || $cat == '') $this->_request->redirectFailure(array('Musisz podać do której kategorii przenieść zdjęcia.'));	
				$model = new AdvNews();
				$model->deleteCategory($this->_id);
				$model->setCategories($this->_id, $cat);
				header('refresh: 3; url=admin,advnews,categories.html');
				$this->view->message = 'Kategoria usunięta pomyślnie ! Zdjęcia przeniesione ! Przekierowywanie...';
				$this->view->render('categories');					
			}	
		}
	}	
	
	public function deactiveAction()
	{
		$model = new AdvNews();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,advnews,categories.html');
		$this->view->message = 'Kategoria wyłączona pomyślnie ! Przekierowywanie...';
		$this->view->render('categories');
	}
		
	public function activeAction()
	{
		$model = new AdvNews();
		$model->active($this->_id);
		header('refresh: 3; url=admin,advnews,categories.html');
		$this->view->message = 'Kategoria włączona pomyślnie ! Przekierowywanie...';
		$this->view->render('categories');
	}	
	# END CATEGORIES
	
	public function deleteAction()
	{
		$model = new AdvNews();
		$model->delete($this->_id);
		header('refresh: 3; url=admin,advnews.html');
		$this->view->message = 'News usunięty pomyślnie ! Przekierowywanie...';
		$this->view->render('edit');
	}
	
	public function newcommentsAction()
	{
		$model = new Comments();
		$this->view->rows = $model->getComments('active = "0" AND module = "advnews"', 'id DESC');		
	}
	
	public function commentsAction()
	{
		$model = new Comments();
		$this->view->rows = $model->getComments('module = "advnews"', 'id DESC LIMIT 50');			
	}	
	
	public function allcommentsAction()
	{
		$model = new Comments();
		$this->view->rows = $model->getComments('module = "advnews"', 'id DESC');			
	}	
	
	public function editcommentAction()
	{
		$model = new Comments();
		$this->view->row = $row = $model->get($this->_id);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}		
		if ($this->_request->isPost()) {
			$filter = new CommentsFilter();
			$filter->filter();
			$model->update($this->_id, $filter->getData());
			header('refresh: 3; url=admin,advnews,comments.html');
			$this->view->message = 'Komentarz zaktualizowany pomyślnie ! Przekierowywanie...';	
		}
	}	
	public function activecommentAction()
	{
		$model = new Comments();
		$model->active($this->_id);
		header('refresh: 3; url=admin,advnews,comments.html');
		$this->view->message = 'Komenatrz zaakceptowany ! Przekierowywanie...';
		$this->view->render('edit');
	}
		
	public function deletecommentAction()
	{
		$model = new Comments();
		$model->delete($this->_id);
		header('refresh: 3; url=admin,advnews,comments.html');
		$this->view->message = 'Komenatrz usunięty pomyślnie ! Przekierowywanie...';
		$this->view->render('edit');
	}	
	
	public function settingsAction()
	{
		$segment = $this->_config->segment('advnews'); 
		
		foreach ($segment as $s)
		{
			$this->view->row[$s['k']] = $s['v'];
		}
		
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			return;
		}
		if ($this->_request->isPost()) {		
			$pp = (int)clear($_POST['per_page']);
			$comments = clear($_POST['comments']);
			$max = (int)clear($_POST['max_chars']);	
			
			if ($pp == '') $this->_request->redirectFailure(array('Pole "Newsów na stronę" jest wymagane.'));
			if ($max == '') $this->_request->redirectFailure(array('Pole "Max znaków komentarza" jest wymagane.'));
							
			$this->_config->update('per_page', $pp, 'advnews');
			$this->_config->update('comments', $comments, 'advnews');
			$this->_config->update('max_chars', $max, 'advnews');

			header('refresh: 3; url=admin,advnews,settings.html');
			$this->view->message = 'Ustawienia zmienione pomyślnie ! Przekierowywanie...';
		}			
	}						
}

?>
