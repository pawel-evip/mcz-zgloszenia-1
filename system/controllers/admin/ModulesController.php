<?php 

class Admin_ModulesController extends Cube_Controller_Abstract
{
	private $_id; 
	
	public function init()
	{	
		$this->view->id = $this->_id = $this->_request->getParam('id');					
	}

	public function indexAction()
	{ 	
		$model = new Modules();
		//$this->view->rows = $model->getModules('m.name <> "modules"', 'name');
		$this->view->rows = $model->getAllModules('name <> "modules"', 'name');
		//print_r($this->view->rows);
		
								
	}
	
	private function _getNotInstalledModules($installed)
	{
		$modules = array('valid' => array(), 'invalid' => array());
		$dir_name = 'system/modules/';	
		$dir_handle = dir($dir_name);
		while (FALSE !== ($fileData = $dir_handle->read()))
		{
			if (is_file($dir_name . $fileData)) {
				if (!array_key_exists(str_replace('.php', '', $fileData), $installed)) 
				{
					include "system/modules/$fileData";			
					// sprawdz czy $fileData = $moduleInfo['name']
					if ($fileData == $moduleInfo['name'].'.php' && $moduleInfo['admin_access'] != '') 
					{
						$modules['valid'][] = $moduleInfo;					
					} 
					else 
						$modules['invalid'][] = $fileData;
					
					unset($moduleInfo);
				}	
			}
		}
		$dir_handle->rewind();	
		$dir_handle->close();
		return $modules;
	}
	
	public function addAction()
	{
		$model = new Modules();
		//$rows = $model->getModules(null, 'name');
		$rows= $model->getAllModules(null,'name');
		$modules = array();
		foreach ($rows as $row)
			$modules[] = $row['name'];
		$this->view->modules = $this->_getNotInstalledModules(array_flip($modules));	
	}	
	
	public function installAction()
	{
		$this->view->render('info');
		$model = new Modules();
		$name = $this->_request->getParam('name');
		if ($name == 'modules' || $name == 'admins' || $name == 'settings') header('Location: admin,modules.html');		
		$result = $model->install($name);
		
		$this->view->error = false;
		if ($result < 1) $this->view->error = true;
		
		switch ($result)
		{
			case 1:  $msg = "Moduł $name zainstalowany pomyślnie !"; break;
			case -1: $msg = "Moduł $name nie został zainstalowany, powód: brak tablicy 'moduleInstall'"; break;
			case -2: $msg = "Moduł $name nie został zainstalowany, powód: nieprawidłowa tablica 'moduleInfo'"; break;
			case -3: $msg = "Moduł $name nie został zainstalowany, powód: brak tablicy 'moduleActions'"; break;
			case -4: $msg = "Moduł $name nie został zainstalowany, powód: nieprawidłowe Controllery modułu"; break;
			case -5: $msg = "Moduł $name nie został zainstalowany, powód: moduł o tej nazwie jest już zainstalowany"; break;
			default: $msg = "Moduł $name nie został zainstalowany, powód: plik system/modules/$name.php nie istnieje"; break;
		}	
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = $msg.' Przekierowywanie...';
	}	

	public function uninstallAction()
	{
		$this->view->render('info');
		$model = new Modules();
		$name = $this->_request->getParam('name');
		if ($name == 'modules' || $name == 'admins' || $name == 'settings') 
			header('Location: admin,modules.html');
		
		//przed usunieciem pobierz id modulu
		$module=$model->getByName($name);
		$id_module=$module['id'];
		$result = $model->uninstall($name);
		
		$this->view->error = false;
		if ($result < 1) $this->view->error = true;
		
		switch ($result)
		{
			case 1:  $msg = "Moduł $name odinstalowany pomyślnie !"; 
				$model = new Modules();
				//usun modul z tabeli admin_access
				$model->unsetAccess(null ,$id_module);
				break;
			case -1: $msg = "Moduł $name nie został odinstalowany, powód: brak tablicy 'moduleUninstall'"; break;
			case -2: $msg = "Moduł $name nie został odinstalowany, powód: moduł nie jest zainstalowany"; break;
			default: $msg = "Moduł $name nie został odinstalowany, powód: plik system/modules/$name.php nie istnieje"; break;
		}	
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = $msg.' Przekierowywanie...';
	}
	
	public function activeAction()
	{
		if ($this->_id == 1) header('Location: admin,modules.html');
		$model = new Modules();
		$model->active($this->_id);
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = 'Moduł aktywowany pomyślnie ! Przekierowywanie...';
		$this->view->render('info');
	}
	
	public function deactiveAction()
	{
		if ($this->_id == 1) header('Location: admin,modules.html');	
		$model = new Modules();
		$model->deactive($this->_id);
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = 'Moduł deaktywowany pomyślnie ! Przekierowywanie...';
		$this->view->render('info');
	}	

	public function accessAction()
	{
		if ($this->_id == 1) header('Location: admin,modules.html');
		$model = new Modules();
		$access = clear($_POST['access']);
		if ($access == 'root' || $access == 'administrator' || $access == 'none')
			$model->setAccess($this->_id, $access);
		header('refresh: 3; url=admin,modules.html');
		$this->view->message = 'Moduł zaktualizowany pomyślnie ! Przekierowywanie...';
		$this->view->render('info');		
	} 		
}

?>
