<?php 

class AdvnewsController extends Cube_Controller_Abstract
{	
	private $_pp;
	private $_max; // max chars
	private $_comments; // true or false
	
# CATEGORIES
	protected function _getCategories()
	{
		$model = new AdvNews();
		$cats = $model->getCategories(null, 'name ASC');
		return $cats;
	}
	


	public function init()
	{
		$this->_pp = $this->_config->get('per_page', 'advnews');
		$this->view->max = $this->_max = $this->_config->get('max_chars', 'advnews');
		$this->view->statusComments = $this->_comments = $this->_config->get('comments', 'advnews');
		
		// struktura z tabela advnews_categories
		$this->view->advnews_categories = $this->_getCategories();
	}

	public function indexAction()
	{
		$model = new AdvNews();
		//$all = $this->_request()->getParam('all', 0);
		$where = null;

		if ($_GET['all'] != 'yes')
			$where = 'cs.id = "1" AND ';
		
		$news = $model->getAll($where.'n.language = "'.Cube_Registry::get('currentLanguage').'" OR n.language = "all"', 'n.id DESC');
		$news_amount = sizeof($news);
		$template = ' <a href="aktualnosci,:value.html">[:id]</a> ';
		$start = $this->_request()->getParam('start', 0);
		
		// PAGES
		Cube_Loader::loadClass('Cube_Pages_Advanced');
		$pages = new Cube_Pages_Advanced($start, $news_amount, $this->_pp, $template);
		
		$tmp = array();
		foreach ($news as $k => $v)
		{
			if ( ($k + 1) >= $start && ($k + 1) <= ($start + $this->_pp) ) 
				$tmp[] = $v;
		}
		
		$this->view->pages = $pages;
		$this->view->rows = $tmp;
		
	}

	public function viewAction()
	{		
		$this->view->id = $id = $this->_request()->getParam('id', 0);
		$model = new AdvNews();
		$news = $model->get($id);
		
		if (!isset($news['add_date'])) $this->view->news = null; 
		else $this->view->news = $news;	
		
		if ($this->_comments) {
			$comments = new Comments();
			$this->view->comments = (array)$comments->getComments('module = "advnews" AND item = "'.$id.'" AND active = "1"', 'id');
		}		
	}

	public function addAction()
	{
		if (!$this->_comments) {
			header('Location: aktualnosci.html');
			return;
		}	
	
		$this->view->id = $id = $this->_request()->getParam('id', 0);
		if ($this->_request->isRedirected()) {
			$this->view->errors = $this->_request->getMessagesFromLastRequest();
			$this->view->row = $this->_request->getParamsFromLastRequest(); 
			$this->view->id = $this->view->row['id'];			
			return;
		}
		if ($this->_request->isPost()) {
			$filter = new CommentsFilter();
			$filter->filter();	
			$data = $filter->getData();
			if (strlen($data['contents']) > $this->_max)
				$this->_request->redirectFailure(array('Podana treść jest zbyt długa, maksymalna ilośc znaków to '.$this->_max));
			$model = new Comments();
			$data['module'] = 'advnews';
			$data['id'] = $id;
			$model->insert($data);	
			header('refresh: 3; url=aktualnosci.html');
			$this->view->message = 'Komentarz dodany pomyślnie ! Przekierowywanie...';
		}
	}
}

?>
