<?php
define('MEDIA_DIR_BIG_PHOTOS', 'public/media/');
define('MEDIA_DIR_THUMBS', 'public/media/thumbs/'); 
define('MEDIA_DIR_MINI', 'public/media/mini/'); 
define('MEDIA_DIR_TMP', 'public/media/tmp/'); 
define('MEDIA_THUMB_WIDTH', 330); 
define('MEDIA_THUMB_HEIGHT', 215); 
define('MEDIA_MWIDTH', 50); 
define('MEDIA_MHEIGHT', 35);
define('MEDIA_DIR', 'public/media/');

require_once 'Cube/Model/Abstract.php';

class Media extends Cube_Model_Abstract
{
	protected $_name = 'media';
	
	public function insert($data)
	{
		$data['add_date'] = time();
		parent::insert($data);
		$this->inc($data['cid']);		
	}
	
 	public function insertPhoto($data)
	{
  		$data['add_date'] = time();
		$sql = 'INSERT INTO media_photos VALUES (null, "'.$data['cid'].'", 
					 "'.$data['add_date'].'", "'.$data['filename'].'", "'.$data['title'].'", "'.$data['description'].'")';
	
	   mysql_query($sql);
  }
    
	public function insertDocs($data)
	{
  		$data['add_date'] = time();
		$sql = 'INSERT INTO media_docs VALUES (null, "'.$data['cid'].'", 
					 "'.$data['add_date'].'", "'.$data['filename'].'", "'.$data['title'].'", "'.$data['description'].'", "'.$data['size'].'")';

	   mysql_query($sql);
	}	
	
	public function delete($id)
	{
		$row = $this->get($id);
 		if($row['filename'] != null)
			if (file_exists('public/media/movie/'.$row['filename'])) unlink('public/movie/movie/'.$row['filename']);
   	//mysql_query('DELETE FROM media WHERE id = "'.$id.'"');  
		$this->dec($row['cid']);	
		parent::delete($id);
	}

	public function update($id, $data)
	{
		$data['oldCid'] = clear($_POST['oldCid']);
		if ($data['cid'] != $data['oldCid'])
		{	
			$this->dec($data['oldCid']);
			$this->inc($data['cid']);
		}
		unset($data['oldCid']);	// usuwa wpis zeby uniknac kolizji w update
		parent::update($id, $data);
	}

	public function updatePhoto($id, $data)
	{
		mysql_query('UPDATE media_photos SET title = "'.$data['title'].'", description = "'.$data['description'].'", cid = "'.$data['cid'].'"
							WHERE id = "'.$id.'"');
	}
	
	public function updateDoc($id, $data)
	{
		mysql_query('UPDATE media_docs SET title = "'.$data['title'].'", description = "'.$data['description'].'", cid = "'.$data['cid'].'"
							WHERE id = "'.$id.'"');
	}
	
	public function get($id)
	{
		$r = mysql_query('SELECT p.*, c.path, c.name FROM media p, media_categories c
		WHERE p.id = "'.$id.'" AND p.cid = c.id');
		if($r != null)
			return mysql_fetch_assoc($r);
		else 
			return null;
	}
	
	public function getAll($where = null, $order = null)
	{
		if (!is_null($where)) $where = ' AND '.$where;
		if (!is_null($order)) $order = ' ORDER BY '.$order;
		$sql = 'SELECT p.*, c.path, c.name, c.id as cat FROM media p, media_categories c
		WHERE p.cid = c.id'.$where.$order;
	
		$r = mysql_query($sql);
		if($r != null)
			return mysql_fetch_all($r);	
		else 
			return null;
	}
	
	public function getCats()
	{
   	$sql = 'SELECT * FROM media_categories';
 		$r = mysql_query($sql);
 		return mysql_fetch_all($r);
 	}
	
	public function getDocs($where = null,$order = null)
	{
	if (!is_null($where)) $where = 'WHERE '.$where;
	if (!is_null($order)) $order = ' ORDER BY '.$order;
	$sql = 'SELECT * FROM media_docs '.$where.$order;
	//echo $sql;	
   	$res = mysql_query($sql);
   	if($res != null){
    		return mysql_fetch_all($res);
  		}else {
    	return null;
  		}
 	}	
	
	public function getPhotos($where = null)
	{
		$sql = 'SELECT * FROM media_photos  '.$where;
   	$res = mysql_query($sql);
   	if($res != null){
    		return mysql_fetch_all($res);
  		}else {
    	return null;
  		}
 	}
 	
 	public function getMovies($where = null)
	{
		$sql = 'SELECT * FROM media  '.$where;
   	$res = mysql_query($sql);
   	if($res != null){
    		return mysql_fetch_all($res);
  		}else {
    	return null;
  		}
 	}
	
	private function inc($cid)
	{
		mysql_query('UPDATE media_categories SET p_amount = p_amount+1 WHERE id = "'.$cid.'"');
	}
	
	private function dec($cid)
	{
		mysql_query('UPDATE media_categories SET p_amount = p_amount-1 WHERE id = "'.$cid.'"');
	}
	
	public function deleteAllPhotos($id)
	{
   	$sql = 'SELECT * FROM media_photos WHERE cid = "'.$id.'"';
   	$res = mysql_query($sql);
   	$res = mysql_fetch_all($res);
   	
   	foreach($res as $r){
    		$this->deletePhoto($r['id']);
 		}
   
 	}
 	
 	public function deleteAllDosc($id)
 	{
   	$sql = 'SELECT * FROM media_docs WHERE cid = "'.$id.'"';
   	$res = mysql_query($sql);
   	$res = mysql_fetch_all($res);
   	
   	foreach($res as $r){
    		$this->deleteDocs($r['id']);
 		}    
  	}
	
	public function deletePhoto($id){
   	$p = $this->getPhotos(' where id = '.$id);
   	if($p['filename'] != null){
			if (file_exists(MEDIA_DIR_BIG_PHOTOS.$p['filename'])) unlink(MEDIA_DIR_BIG_PHOTOS.$p['filename']);
			if (file_exists(MEDIA_DIR_THUMBS.$p['filename'])) unlink(SMEDIA_DIR_THUMBS.$p['filename']);
			if (file_exists(MEDIA_DIR_MINI.$p['filename'])) unlink(MEDIA_DIR_MINI.$p['filename']); 	
		}
   	mysql_query('DELETE FROM media_photos WHERE id = "'.$id.'"');
 	}
 	
 	public function deleteDocs($id) {
    	$p = $this->getDocs(' where id = '.$id);
    	if($p['filename'] != null)
			if (file_exists('public/media/docs/'.$p['filename'])) unlink('public/media/docs/'.$p['filename']);
			
		$sql = 'DELETE FROM media_docs WHERE id = "'.$id.'"';
		//echo $sql;
		//exit;	
   	mysql_query($sql);   
  	}
		
}
					 
?>
