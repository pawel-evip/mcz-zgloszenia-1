<?php

require_once 'Cube/Model/Abstract.php';

class EntryCategories extends Cube_Model_Abstract
{
	protected $_name = 'entry_categories';

	public function insert($data)
	{
		$data['active'] = '1';
		$data['s_amount'] = '0';
		$data['p_amount'] = '0';
		parent::insert($data);
		//echo mysql_errno() . ": " . mysql_error(). "\n";
		$this->inc($data['paren_id']);
	}

	public function getInfo($id)
	{
   	$sql = 'SELECT * FROM entry_categories WHERE id = "'.$id.'"';
	//echo $sql;
   	$res = mysql_query($sql);
   	if($res != null){
    		return mysql_fetch_assoc($res);
 		}else
 			return null;
 	}

	private function inc($id)
	{
		mysql_query('UPDATE entry_categories SET s_amount = s_amount+1 WHERE id = "'.$id.'"');
	}

	private function dec($id)
	{
		mysql_query('UPDATE entry_categories SET s_amount = s_amount-1 WHERE id = "'.$id.'"');
	}

	public function setCategories($oldId, $newId) // masowa zamiana kategorii dla zdjęć
	{
		mysql_query('UPDATE entry_photos SET cid = "'.$newId.'" WHERE cid = "'.$oldId.'"');
		$amount = mysql_affected_rows(); // ilość przetworzonych wirszy
		mysql_query('UPDATE entry_categories SET p_amount = p_amount+'.$amount.' WHERE cid = "'.$newId.'"');

		# zmniejsz ilość subkategori w kategori starej
		$r = mysql_query('SELECT parent_id FROM entry_categories WHERE id = "'.$oldId.'"');
		$row = mysql_fetch_assoc($r);
		mysql_query('UPDATE entry_categories SET s_amount = s_amount-1 WHERE id = "'.$row['parent_id'].'"');

		#zwieksz ilość subkategori w kategori nowej
		$r = mysql_query('SELECT parent_id FROM entry_categories WHERE id = "'.$newId.'"');
		$row = mysql_fetch_assoc($r);
		mysql_query('UPDATE entry_categories SET s_amount = s_amount+1 WHERE id = "'.$row['parent_id'].'"');
	}

	public function delete($id)
	{
		parent::delete($id);
		mysql_query('DELETE FROM entry_categories WHERE parent_id = "'.$id.'"');
		mysql_query('DELETE FROM entry WHERE cid = "'.$id.'"');

	}

	public function active($cid)
	{
		mysql_query('UPDATE entry_categories SET active = "1" WHERE id = "'.$cid.'"');
	}

	public function deactive($cid)
	{
		mysql_query('UPDATE entry_categories SET active = "0" WHERE id = "'.$cid.'"');
	}

	static public function getSubcategories($parent_id)
	{
		$r = mysql_query('SELECT * FROM entry_categories WHERE parent_id = "'.$parent_id.'"');
		return mysql_fetch_all($r);
	}

	static public function getAllIds($parentId, $ids = array()) {
		$subIds = self::getSubcategories($parentId);
		foreach($subIds as $subId) {
			array_push($ids, $subId['id']);
			$ids = self::getAllIds($subId['id'],$ids);
		}
		return $ids;
	}

	public function getLastPhoto($cid)
	{
		$r = mysql_query('SELECT filename FROM entry_photos WHERE cid = "'.$cid.'" ORDER BY id DESC LIMIT 1');
		return mysql_fetch_assoc($r);
	}

	public function getPath($id)
	{
		$r = mysql_query('SELECT * FROM entry_categories WHERE id = "'.$id.'"');
		return mysql_fetch_assoc($r);
	}

		public function deleteAllPhotos($id)
	{
   	$sql = 'SELECT * FROM entry_photos WHERE cid = "'.$id.'"';
   	$res = mysql_query($sql);
   	$res = mysql_fetch_all($res);

   	foreach($res as $r){
    		$this->deletePhoto($r['id']);
 		}

 	}

 	public function deleteAllDocs($id)
 	{
   	$sql = 'SELECT * FROM entry_docs WHERE cid = "'.$id.'"';
   	$res = mysql_query($sql);
   	$res = mysql_fetch_all($res);

   	foreach($res as $r){
    		$this->deleteDocs($r['id']);
 		}
  	}

	public function deletePhoto($id){
   	$p = $this->getPhotos('where id = '.$id);
   	if($p['filename'] != null){
			if (file_exists(ENTRY_DIR_BIG_PHOTOS.$p['filename'])) unlink(ENTRY_DIR_BIG_PHOTOS.$p['filename']);
			if (file_exists(MENTRY_DIR_THUMBS.$p['filename'])) unlink(ENTRY_DIR_THUMBS.$p['filename']);
			if (file_exists(ENTRY_DIR_MINI.$p['filename'])) unlink(ENTRY_DIR_MINI.$p['filename']);
		}
   	mysql_query('DELETE FROM entry_photos WHERE id = "'.$id.'"');
 	}

 	public function deleteDocs($id) {
    	$p = $this->getDocs('where id = '.$id);
    	if($p['filename'] != null)
			if (file_exists('public/entry/docs/'.$p['filename'])) unlink('public/entry/docs/'.$p['filename']);
   	mysql_query('DELETE FROM entry_docs WHERE id = "'.$id.'"');
  	}

}

?>
