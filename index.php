<?php
/**
 *	CMS for Evip, www.e-vip.com.pl
 *	Author: Michał Daniel, Cube
 *	www.icube.pl 
 *  02-03.2008 
 */
// Ustawienia PHP
ini_set('display_errors','Off');
ini_set('magic_quotes_gpc','Off');
ini_set('register_globals','Off');

date_default_timezone_set('Europe/Warsaw');
set_include_path('.' 
	.  PATH_SEPARATOR . './libs/'
	.  PATH_SEPARATOR . './system/models/'
	.  PATH_SEPARATOR . './system/filters/'
	.  PATH_SEPARATOR . get_include_path());

try {	

// Ustawia autoload w Cube_Loader
require_once 'Cube/Loader.php';
Cube_Loader::registerAutoload();

require_once 'Cube/Exception.php';

// Sprawdz magic quotes
require_once 'Cube/Functions.php';
checkMagicQuotes();

// Załaduj konfiguracje
require_once 'config.php';

// Załaduj rejestr
require_once 'Cube/Registry.php';
$registry = Cube_Registry::getInstance();
$registry->set('systemTemplates', $systemTemplates);

// Załaduj obiekt request
require_once 'Cube/Request.php';
$request = new Cube_Request();

// Załaduj sterownik bazy danych
require_once 'Cube/DbMysql.php';
$dbAdapter = Cube_DbMysql::getInstance($dbSettings);

// Załaduj Config
require_once 'Cube/Config.php';
$config = Cube_Config::getInstance();

// Rejestruj 
$registry->set('config', $config);
$registry->set('request', $request);
$registry->set('dbAdapter', $dbAdapter);
$registry->set('systemLanguages', $systemLanguages);

require_once 'Cube/UserSession.php';
$session = new Cube_UserSession();
$session->impress();
$session->setTheme($request->getTheme());


$registry->set('session', $session);
$registry->set('currentLanguage', $session->getLanguage());

// Załaduj fron controller
require_once 'Cube/Controller/Front.php';
$front = Cube_Controller_Front::getInstance();
$front->dispatch();

}
catch (Exception $e)
{
	echo $e->getMessage();
}
					 
?>
